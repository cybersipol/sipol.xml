﻿using NSubstitute;

using NUnit.Framework;

using Sipol.XML.Interfaces;

using System.Xml;

namespace Sipol.XML.Tests
{
    [TestFixture()]
    public class XmlDocumentMainTagReader_Tests
    {

        private XmlDocumentMainTagReader sut;
        private IXmlDocumentGetter xmlDocumentGetter;
        private string expectedMainTag = "root";


        [SetUp]
        public void Init()
        {
            xmlDocumentGetter = Substitute.For<IXmlDocumentGetter>();
            sut = null;
        }

        #region GetMainTag Tests


        [Test()]
        public void GetMainTag__byDefault__ResultIsEqualToMainTagFromXmlDocument()
        {
            // Arrange
            xmlDocumentGetter.XmlDocument.Returns(CreateDefaultDocument());
            sut = CreateSUT(xmlDocumentGetter);
            // Act

            var result = sut.GetMainTag();

            // Assert
            Assert.That(result,
                        Is.EqualTo(expectedMainTag)
            );
        }

        [Test()]
        public void GetMainTag__byDefault__GetXmlDocumentFromXmlDocumentGetter()
        {
            // Arrange
            xmlDocumentGetter.XmlDocument.Returns(CreateDefaultDocument());
            sut = CreateSUT(xmlDocumentGetter);
            // Act

            var result = sut.GetMainTag();

            // Assert
            var xmldoc = xmlDocumentGetter.Received().XmlDocument;
        }

        [Test()]
        public void GetMainTag__WhenXmlDocIsNull__EmptyString()
        {
            // Arrange
            xmlDocumentGetter.XmlDocument.Returns(null as XmlDocument);
            sut = CreateSUT(xmlDocumentGetter);
            // Act

            var result = sut.GetMainTag();

            // Assert
            Assert.That(result,
                        Is.Not.Null &
                        Is.Empty
            );
        }


        [Test()]
        public void GetMainTag__WhenXmlDocHasRootElementWithNamespace__LocalTagNameResult()
        {
            // Arrange
            XmlDocument doc = new XmlDocument();
            doc.LoadXml("<ns:"+expectedMainTag+" xmlns:ns=\"http://test.org.pl\"/>");
            xmlDocumentGetter.XmlDocument.Returns(doc);
            sut = CreateSUT(xmlDocumentGetter);

            // Act
            var result = sut.GetMainTag();

            // Assert
            Assert.That(result,
                        Is.EqualTo(expectedMainTag)
            );
        }


        [Test()]
        public void GetMainTag__WhenSutCreatedWithXmlDocument__MainTagNameResult()
        {
            // Arrange
            XmlDocument doc = CreateDefaultDocument();
            sut = CreateSUT(doc);

            // Act
            var result = sut.GetMainTag();

            // Assert
            Assert.That(result,
                        Is.EqualTo(expectedMainTag)
            );
        }

        [Test()]
        public void GetMainTag__WhenSutCreatedWithXmlDocumentWithNamespace__MainTagNameResultIsLocalName()
        {
            // Arrange
            XmlDocument doc = new XmlDocument();
            doc.LoadXml("<ns:" + expectedMainTag + " xmlns:ns=\"http://test.org.pl\"/>");
            sut = CreateSUT(doc);

            // Act
            var result = sut.GetMainTag();

            // Assert
            Assert.That(result,
                        Is.EqualTo(expectedMainTag)
            );
        }



        #endregion


        #region CreateXmlDocumentGetter Tests


        [Test()]
        public void CreateXmlDocumentGetter__byDefault__InstanceOfIXmlDocumentGetter()
        {
            // Arrange
            XmlDocument doc = CreateDefaultDocument();
            sut = CreateSUT(doc);
            // Act
            var result = sut.CreateXmlDocumentGetter();

            // Assert
            Assert.That(result,
                        Is.Not.Null &
                        Is.InstanceOf<IXmlDocumentGetter>()
            );
        }

        [Test()]
        public void CreateXmlDocumentGetter__byDefault__PropertyXmlDocumentGetterIsSameAsCreated()
        {
            // Arrange
            XmlDocument doc = CreateDefaultDocument();
            sut = CreateSUT(doc);
            // Act
            var result = sut.CreateXmlDocumentGetter();

            // Assert
            Assert.That(result,
                           Is.SameAs(sut.XmlDocumentGetter)
            );
        }

        #endregion

        #region help funcs

        private XmlDocumentMainTagReader CreateSUT( IXmlDocumentGetter xmlGetter)
        {
            sut = new XmlDocumentMainTagReader(xmlGetter);
            return sut;
        }

        private XmlDocumentMainTagReader CreateSUT() => CreateSUT(xmlDocumentGetter);

        private XmlDocumentMainTagReader CreateSUT( XmlDocument xmldoc )
        {
            sut = new XmlDocumentMainTagReader(xmldoc);
            return sut;
        }


        private XmlDocument CreateDefaultDocument()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml("<"+expectedMainTag+"><item/></"+expectedMainTag+">");
            return doc;
        }

        #endregion

    }
}