﻿using NSubstitute;
using NUnit.Framework;
using Sipol.XML.Interfaces;
using System.Collections.Generic;
using System.Xml;

namespace Sipol.XML.Abstracts.Tests
{
    [TestFixture()]
    public class XmlDocsAdderByXPath_Tests
    {
        private SUTMock sut;

        [SetUp]
        public void Init()
        {
            sut = null;
        }


        #region ctor Tests


        [Test()]
        public void ctor__NullFirstDocument__ThrowException()
        {
            Assert.That(() => 
            {
                object result = new SUTMock(null, new XmlDocument());
            },
            Throws.ArgumentNullException
                
            );
        }


        [Test()]
        public void ctor__NullSecondDocument__ThrowException()
        {
            Assert.That(() =>
            {
                object result = new SUTMock(new XmlDocument(), null);
            },
            Throws.ArgumentNullException

            );
        }


        [Test()]
        public void ctor()
        {
            object result = new SUTMock(new XmlDocument(), new XmlDocument());

            Assert.That(result,
                        Is.Not.Null &
                        Is.InstanceOf<XmlDocsAdderByXPath>()
                        );
        }


        [Test()]
        public void ctor__NullFactory__IsCalledBaseCreateDefaultXPathNodeFactory()
        {
            sut = new SUTMock(new XmlDocument(), new XmlDocument(), null);

            Assert.That(sut.count_CreateDefaultXPathNodeFactory,
                        Is.EqualTo(1)
                        );


        }


        #endregion



        #region CreateDefaultXPathNodeFactory Tests


        [Test()]
        public void CreateDefaultXPathNodeFactory__NullFactory__FactoryIsXPathXmlNodeFactory()
        {
            sut = new SUTMock(new XmlDocument(), new XmlDocument(), null);

            sut.Call_Base_CreateDefaultXPathNodeFactory();

            Assert.That(sut.XPathNodeFactory,
                        Is.Not.Null &
                        Is.InstanceOf<XPathXmlNodeFactory>()
                        );
        }


        [Test()]
        public void CreateDefaultXPathNodeFactory__CreateFactory__FactorySameAsCreated()
        {
            IXPathXmlNodeFactory factory = Substitute.For<IXPathXmlNodeFactory>();

            sut = new SUTMock(new XmlDocument(), new XmlDocument(), factory);

            sut.Call_Base_CreateDefaultXPathNodeFactory();

            Assert.That(sut.XPathNodeFactory,
                        Is.Not.Null &
                        Is.InstanceOf<IXPathXmlNodeFactory>() &
                        Is.SameAs(factory)
                        );
        }

        #endregion



        #region CreateXPathFromXmlDocument Tests

        [Test()]
        public void CreateXPathFromXmlDocument__NullDocument__EmptyXPathList()
        {
            XmlDocument doc1 = new XmlDocument();
            doc1.LoadXml("<root><item1/><item2><item3/></item2></root>");
            XmlDocument doc2 = new XmlDocument();
            //doc2.LoadXml("<root><item1>test item1</item1><item2><item3>test item3</item3></item2></root>");

            XPathXmlNodeShortFactory factory = new XPathXmlNodeShortFactory();

            sut = new SUTMock(doc1, doc2, factory);

            sut.Base_XPathDocList.Add("aaa", doc1.CreateElement("aaa"));

            sut.Call_Base_CreateXPathFromXmlDocument(null);


            Assert.That(sut.Base_XPathDocList.Keys,
                            Is.Not.Null &
                            Is.Empty 
                            );

        }

        [Test()]
        public void CreateXPathFromXmlDocument__SimpleDocumentAndShortNodeFactory__CorrectXPathInList()
        {
            XmlDocument doc1 = new XmlDocument();
            doc1.LoadXml("<root><item1/><item2><item3/></item2></root>");
            XmlDocument doc2 = new XmlDocument();
            //doc2.LoadXml("<root><item1>test item1</item1><item2><item3>test item3</item3></item2></root>");

            XPathXmlNodeShortFactory factory = new XPathXmlNodeShortFactory();

            sut = new SUTMock(doc1, doc2, factory);

            sut.Call_Base_CreateXPathFromXmlDocument(doc1);


            Assert.That(sut.Base_XPathDocList.Keys,
                            Is.Not.Null &
                            Is.Not.Empty &
                            Is.EquivalentTo(new string[] { "/root", "/root/item1", "/root/item2", "/root/item2/item3" })
                            );
            
        }


        [Test()]
        public void CreateXPathFromXmlDocument__SimpleDocumentWithTextNodesAndShortNodeFactory__CorrectXPathInList()
        {
            XmlDocument doc1 = new XmlDocument();
            doc1.LoadXml("<root><item1/><item2><item3>test item3</item3></item2></root>");
            XmlDocument doc2 = new XmlDocument();

            XPathXmlNodeShortFactory factory = new XPathXmlNodeShortFactory();

            sut = new SUTMock(doc1, doc2, factory);

            sut.Call_Base_CreateXPathFromXmlDocument(doc1);


            Assert.That(sut.Base_XPathDocList.Keys,
                            Is.Not.Null &
                            Is.Not.Empty &
                            Is.EquivalentTo(new string[] { "/root", "/root/item1", "/root/item2", "/root/item2/item3" })
                            );

        }



        #endregion




        #region Mock class

        private class SUTMock : XmlDocsAdderByXPath
        {
            public int count_CreateDefaultXPathNodeFactory { get; private set; } = 0;

            public SUTMock( XmlDocument firstXmlDocument, XmlDocument secondXmlDocument, IXPathXmlNodeFactory xPathNodeFactory = null ) : base(firstXmlDocument, secondXmlDocument, xPathNodeFactory)
            {
            }

            public void Call_Base_CreateDefaultXPathNodeFactory() => CreateDefaultXPathNodeFactory();
            public void Call_Base_CreateXPathFromXmlDocument( XmlDocument doc ) => CreateXPathFromXmlDocument(doc);

            public Dictionary<string, XmlNode> Base_XPathDocList => XPathDocList; 

            protected override void CreateDefaultXPathNodeFactory()
            {
                count_CreateDefaultXPathNodeFactory++;
                base.CreateDefaultXPathNodeFactory();
            }

            public override void AdditionXmlDocuments()
            {
                return;
            }
        }

        #endregion
    }
}