﻿using NUnit.Framework;
using Sipol.XML.Interfaces;
using System;
using System.Collections.Generic;
using System.Xml;

namespace Sipol.XML.Tests
{
    [TestFixture()]
    public class XmlDocsAdderByXPathShort_Tests
    {

        private SUTMock sut;

        [SetUp]
        public void Init()
        {
            sut = null;
        }

        #region AdditionXmlDocuments Tests


        [Test()]
        public void AdditionXmlDocuments__CreateXPathList__NotEmptyXPathList()
        {
            //Arrange
            XmlDocument doc1 = new XmlDocument();
            doc1.LoadXml("<root><item1/><item2><item3/></item2></root>");
            XmlDocument doc2 = new XmlDocument();
            doc2.LoadXml("<root><item1>test1 for item1</item1><item1>test2  for item1</item1><item2><item3>test for item3</item3></item2></root>");

            sut = new SUTMock(doc1, doc2);

            // Act
            sut.AdditionXmlDocuments();


            //Assert
            Assert.That(sut.XPathList,
                        Is.Not.Null &
                        Is.Not.Empty
            );
        }


        [Test()]
        public void AdditionXmlDocuments__EmptyFirstDocument__NoCallEventOnNodeAddition()
        {
            //Arrange

            XmlDocument doc1 = new XmlDocument();
            
            XmlDocument doc2 = new XmlDocument();
            doc2.LoadXml("<root><item1>test1 for item1</item1><item1>test2  for item1</item1><item2><item3>test for item3</item3></item2></root>");

            int countItem1 = 0;

            sut = new SUTMock(doc1, doc2);
            sut.OnNodeAddition += ( XmlNode node1, XmlNode node2 ) => { countItem1++; };

            // Act
            sut.AdditionXmlDocuments();



            // Assert
            Assert.That(countItem1,
                            Is.EqualTo(0)
                    );

        }


        [Test()]
        public void AdditionXmlDocuments__EmptySecondDocument__NoCallEventOnNodeAddition()
        {
            //Arrange

            XmlDocument doc1 = new XmlDocument();
            doc1.LoadXml("<root><item1/><item2><item3/></item2></root>");
            XmlDocument doc2 = new XmlDocument();
            

            int countItem1 = 0;

            sut = new SUTMock(doc1, doc2);
            sut.OnNodeAddition += ( XmlNode node1, XmlNode node2 ) => { countItem1++; };

            // Act
            sut.AdditionXmlDocuments();



            // Assert
            Assert.That(countItem1,
                            Is.EqualTo(0)
                    );

        }


        [Test()]
        public void AdditionXmlDocuments__NoEquivalentXPathInXmlDocs__NoCallEventOnNodeAddition()
        {
            //Arrange

            XmlDocument doc1 = new XmlDocument();
            doc1.LoadXml("<root><item12/><item22><item3/></item22></root>");
            XmlDocument doc2 = new XmlDocument();
            doc2.LoadXml("<noroot><item1>test1 for item1</item1><item1>test2  for item1</item1><item2><item3>test for item3</item3></item2></noroot>");

            int countItem1 = 0;

            sut = new SUTMock(doc1, doc2);
            sut.OnNodeAddition += ( XmlNode node1, XmlNode node2 ) => { countItem1++; };

            // Act
            sut.AdditionXmlDocuments();



            // Assert
            Assert.That(countItem1,
                            Is.EqualTo(0)
                    );

        }




        [Test()]
        public void AdditionXmlDocuments__AddTwoNodesItem1ToFirstXmlDocument__Item1ValueAddTwice()
        {
            //Arrange

            XmlDocument doc1 = new XmlDocument();
            doc1.LoadXml("<root><item1/><item2><item3/></item2></root>");
            XmlDocument doc2 = new XmlDocument();
            doc2.LoadXml("<root><item1>test1 for item1</item1><item1>test2  for item1</item1><item2><item3>test for item3</item3></item2></root>");

            int countItem1 = 0;

            sut = new SUTMock(doc1, doc2);
            sut.OnNodeAddition += (XmlNode node1, XmlNode node2) => { if (node2.LocalName == "item1") countItem1++; };

            // Act
            sut.AdditionXmlDocuments();



            // Assert
            Assert.That(countItem1,
                            Is.EqualTo(2)
                    );

        }

        private void Sut_OnNodeAddition( XmlNode arg1, XmlNode arg2 )
        {
            throw new NotImplementedException();
        }


        #endregion



        #region CreateDefaultXPathNodeFactory Tests


        [Test()]
        public void CreateDefaultXPathNodeFactory__byDefault__XPathNodeFactoryTypeIsXPathXmlNodeShortFactory()
        {
            XmlDocument doc1 = new XmlDocument();
            doc1.LoadXml("<root><item1/><item2><item3/></item2></root>");
            XmlDocument doc2 = new XmlDocument();
            doc2.LoadXml("<root><item1>test item1</item1><item2><item3>test item3</item3></item2></root>");

            sut = new SUTMock(doc1, doc2);

            sut.Call_CreateDefaultXPathNodeFactory();

            Assert.That(sut.XPathNodeFactory,
                        Is.Not.Null &
                        Is.InstanceOf<XPathXmlNodeShortFactory>()
                );
        }


        #endregion

        #region Mock class

        private class SUTMock : XmlDocsAdderByXPathShort
        {
            public SUTMock( XmlDocument firstXmlDocument, XmlDocument secondXmlDocument, IXPathXmlNodeFactory xPathNodeFactory = null ) : base(firstXmlDocument, secondXmlDocument, xPathNodeFactory)
            {
            }

            public Dictionary<string, XmlNode> XPathList => base.XPathDocList;


            public void Call_CreateDefaultXPathNodeFactory() => CreateDefaultXPathNodeFactory();

        }

        #endregion
    }
}