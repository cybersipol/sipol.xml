﻿using NUnit.Framework;

namespace Sipol.XML.Tests
{
    [TestFixture()]
    public class XmlTagName_Tests
    {
        private XmlTagNameWithType sut;

        [SetUp]
        public void Init()
        {
            sut = null;
        }


        #region TagName Tests

        [TestCase("sometag","sometag")]
        [TestCase("__sometag", "__sometag")]
        [TestCase("__some tag", "__sometag")]
        [TestCase("112__sometag", "__sometag")]
        [TestCase(":__some_tag", "__some_tag")]
        [TestCase("hh:ns:__some_tag", "hhns:__some_tag")]
        public void TagName__byDefault__CorrectTagNameOrEmpty(string tagName, string expectedTagName)
        {
            // Arrange
            sut = CreateSUT(tagName);
            // Act
            string result = sut.TagName;


            // Assert
            Assert.That(result,
                        Is.EqualTo(expectedTagName)
                        );
                        
        }


        #endregion

        #region Help funcs

        private XmlTagNameWithType CreateSUT()
        {
            sut = new XmlTagNameWithType();
            return sut;
        }

        private XmlTagNameWithType CreateSUT( string tagname )
        {
            sut = CreateSUT();
            sut.TagName = tagname;
            return sut;
        }


        #endregion
    }
}