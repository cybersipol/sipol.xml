﻿using NSubstitute;

using NUnit.Framework;

using Sipol.IO;
using Sipol.XML.Unit.Tests;
using System.IO;
using System.IO.Abstractions;
using System.Xml;


namespace Sipol.XML.Tests
{
    [TestFixture()]
    public class XmlFileDocumentatorWithoutNamespace_Tests
    {
        private XmlFileDocumentatorWithoutNamespace sut;

        [SetUp]
        public void Init()
        {
            FS.Current = new SipolFileSys(true);
            FS.File = Substitute.For<IFile>();

            sut = null;
        }

        #region XmlDocument Tests


        [Test()]
        public void XmlDocument__byDefault__InstanceOfXmlDocument()
        {
            // Arrange
            string path = @"Z:\test\xmlfiles\some.xml";

            FS.File.Exists(path).Returns(true);
            FS.File.ReadAllText(path).Returns("<root/>");

            sut = CreateSUT(path);

            // Act
            var result = sut.XmlDocument;

            // Assert
            Assert.That(result,
                        Is.Not.Null &
                        Is.InstanceOf<XmlDocument>()
            );

        }


        [Test()]
        public void XmlDocument__byDefault__SelectSingleNodeWithoutNamespacePrefix()
        {
            // Arrange
            string path = @"Z:\test\xmlfiles\some.xml";

            FS.File.Exists(path).Returns(true);
            string xmlstring = Xmls.GetXmlStringWithNS();
            FS.File.ReadAllText(path).Returns(xmlstring);

            sut = CreateSUT(path);

            // Act
            var result = sut.XmlDocument;

            // Assert
            string xpath = Xmls.GetXPathKrsWithoutNSFromXmlStringWithNS();

            var node = result.SelectSingleNode(xpath);

            Assert.That(node,
                        Is.Not.Null &
                        Is.InstanceOf<XmlNode>()
            );

            Assert.That(node,
                            Has.Property("InnerText").Match("^([0-9]{10})$")
            );



        }


        [Test()]
        public void XmlDocument__byDefault__SelectNodesWithoutNamespacePrefix()
        {
            // Arrange
            string path = @"Z:\test\xmlfiles\some.xml";

            FS.File.Exists(path).Returns(true);
            string xmlstring = Xmls.GetXmlStringWithNS();
            FS.File.ReadAllText(path).Returns(xmlstring);

            sut = CreateSUT(path);

            // Act
            var result = sut.XmlDocument;



            // Assert
            string xpath = Xmls.GetXPathKrsWithoutNSFromXmlStringWithNS();

            var nodes = result.SelectNodes(xpath);

            foreach (var node in nodes)
            {

                Assert.That(node,
                            Is.Not.Null &
                            Is.InstanceOf<XmlNode>()
                );

                Assert.That(node,
                                Has.Property("InnerText").Match("^([0-9]{10})$")
                );

            }



        }


        [Test()]
        public void XmlDocument__byDefault__CallFSFileExistsOnXmlFilePath()
        {
            // Arrange
            string path = @"Z:\test\xmlfiles\some.xml";

            FS.File.Exists(path).Returns(true);
            FS.File.ReadAllText(path).Returns("<root/>");

            sut = CreateSUT(path);

            // Act
            var result = sut.XmlDocument;

            // Assert
            FS.File.Received(1).Exists(path);
        }


        [Test()]
        public void XmlDocument__WhenFileNotExists__ThrowFileNotFoundException()
        {
            // Arrange
            string path = @"Z:\test\xmlfiles\some.xml";

            FS.File.Exists(path).Returns(false);

            sut = CreateSUT(path);

            // Act
            // Assert
            Assert.That(() =>
            {
                var result = sut.XmlDocument;
            },
                Throws.InstanceOf<FileNotFoundException>()
            );
        }

        [Test()]
        public void XmlDocument__byDefault__CallFSFileReadAllTextOnXmlFilePath()
        {
            // Arrange
            string path = @"Z:\test\xmlfiles\some.xml";

            FS.File.Exists(path).Returns(true);
            FS.File.ReadAllText(path).Returns("<root/>");

            sut = CreateSUT(path);

            // Act
            var result = sut.XmlDocument;

            // Assert
            FS.File.Received(1).ReadAllText(path);
        }


        #endregion


        #region Make Tests


        [Test()]
        public void Make__byDefault__InstanceNotNullAndXmlFileDocumentatorWithoutNamespacePrefix()
        {
            // Arrange
            string path = @"Z:\test\xmlfiles\some.xml";
            // Act
            var result = XmlFileDocumentatorWithoutNamespace.Make(path);

            // Assert
            Assert.That(result,
                        Is.Not.Null &
                        Is.InstanceOf<XmlFileDocumentatorWithoutNamespace>()
            );
        }

        [TestCase(null)]
        [TestCase("")]
        [TestCase(" ")]
        [TestCase("   ")]
        public void Make__WhenXmlFilePathIsIncorrectString__ThrowArgumentNullExcception(string path)
        {
            // Arrange
            // Act
            // Assert
            Assert.That(() =>
                {
                    var result = XmlFileDocumentatorWithoutNamespace.Make(path);
                },
                Throws.ArgumentNullException
            );
        }

        #endregion

        #region help funcs

        private XmlFileDocumentatorWithoutNamespace CreateSUT( string xmlfilepath )
        {
            sut = new XmlFileDocumentatorWithoutNamespace(xmlfilepath);
            return sut;
        }


        #endregion
    }
}