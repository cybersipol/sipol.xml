﻿using InfoVeriti.Net.Extensions.Xml;
using NUnit.Framework;

using System;
using System.Xml;

namespace Sipol.XML.Tests
{
    [TestFixture()]
    public class XPathInfo_Tests
    {

        #region Path Tests

        [Test()]
        public void Path_byDefault_NotNullAndNotEmpty()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(@"<root><child1><child2>Hello</child2></child1><child3/></root>");
            XmlElement root = doc.DocumentElement;
            XmlNode node = doc.DocumentElement.SelectSingleNode("/root/child1/child2");


            XPathXmlNode path = new XPathXmlNode(node);

            Assert.That(path.XPath, Is.Not.Null & Is.Not.Empty);
        }

        [Test()]
        public void Path_NullNode_ThrowException()
        {
            Assert.That(() => { XPathXmlNode path = new XPathXmlNode(null);  }, Throws.InstanceOf<ArgumentNullException>() );
        }

        [Test()]
        public void Path_NodeNotElement_ThowExcprtion()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(@"<root><child1><child2>Hello</child2></child1><child3/></root>");
            XmlElement root = doc.DocumentElement;
            XmlNode node = doc.CreateNode(XmlNodeType.Comment, "aaa", "");
            Assert.That(() => { XPathXmlNode path = new XPathXmlNode(node); }, Throws.InstanceOf<TypeAccessException>());
        }


        [Test()]
        public void Path_UseIndexerAndNotUseLocalNames_PathWithIndexAndNamespacePrefix()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(@"<root xmlns=""A"" xmlns:ns=""B""><ns:child1><child2>Hello</child2><child2>Hello2</child2></ns:child1><ns:child3/></root>");

            XmlElement root = doc.DocumentElement;

            doc.IterateThroughAllNodes(delegate (XmlNode node)
            {
                if (node.LocalName.Equals("child2"))
                {
                    XPathXmlNode path = new XPathXmlNode(node);

                    Assert.That(path.XPath, Is.EqualTo("/root[1]/ns:child1[1]/child2[1]") | Is.EqualTo("/root[1]/ns:child1[1]/child2[2]"));
                }
            });
        }
        #endregion

        #region IndexInParent Tests

        [Test()]
        public void IndexInParent_byDefault_ProperIndex()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(@"<root xmlns=""A"" xmlns:ns=""B""><ns:child1><child2>Hello</child2><child2>Hello2</child2></ns:child1><ns:child3/></root>");

            XmlElement root = doc.DocumentElement;
            XmlElement node = (XmlElement) root.ChildNodes[0].ChildNodes[1];

            XPathXmlNode xpathnode = new XPathXmlNode(node);

            string xpath = xpathnode.XPath;

            Assert.That(xpathnode.IndexInParent, Is.EqualTo(2));
        }

        [Test()]
        public void IndexInParent_RootElement_IndexEqualOne()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(@"<root xmlns=""A"" xmlns:ns=""B""><ns:child1><child2>Hello</child2><child2>Hello2</child2></ns:child1><ns:child3/></root>");

            XmlElement root = doc.DocumentElement;

            XPathXmlNode xpathnode = new XPathXmlNode(root);

            string xpath = xpathnode.XPath;

            Assert.That(xpathnode.IndexInParent, Is.EqualTo(1));
        }


        #endregion

        #region NamespacePrefix Tests

        [Test()]
        public void NamespacePrefix_byDefault_ProperPrefix()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(@"<root xmlns=""A"" xmlns:ns=""B""><ns:child1><ns:child2>Hello</ns:child2><ns:child2>Hello2</ns:child2></ns:child1><ns:child3/></root>");

            XmlElement root = doc.DocumentElement;
            XmlElement node = (XmlElement)root.ChildNodes[0].ChildNodes[1];

            XPathXmlNode xpathnode = new XPathXmlNode(node);

            string xpath = xpathnode.XPath;

            Assert.That(xpathnode.NamespacePrefix, Is.EqualTo("ns"));
        }

        [Test()]
        public void NamespacePrefix_NoNamespacePrefix_EmptyPrefix()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(@"<root xmlns=""A"" xmlns:ns=""B""><ns:child1><child2>Hello</child2><child2>Hello2</child2></ns:child1><ns:child3/></root>");

            XmlElement root = doc.DocumentElement;
            XmlElement node = (XmlElement)root.ChildNodes[0].ChildNodes[1];

            XPathXmlNode xpathnode = new XPathXmlNode(node);

            string xpath = xpathnode.XPath;

            Assert.That(xpathnode.NamespacePrefix, Is.Not.Null & Is.Empty);
        }

        #endregion
    }
}