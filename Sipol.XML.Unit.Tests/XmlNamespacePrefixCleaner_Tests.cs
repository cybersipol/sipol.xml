﻿using NSubstitute;
using NUnit.Framework;
using Sipol.XML.Interfaces;
using Sipol.XML.Unit.Tests;

namespace Sipol.XML.Tests
{
    [TestFixture()]
    public class XmlNamespacePrefixCleaner_Tests
    {
        private XmlNamespacePrefixCleaner sut;
        private IXmlStringTransformer transformer;


        [SetUp]
        public void Init()
        {
            transformer = null;
            sut = null;
        }

        #region Make Tests


        [Test()]
        public void Make__byDefault__NotNullAndInstanceOfXmlNamespacePrefixCleaner()
        {
            // Arrange


            // Act
            var result = XmlNamespacePrefixCleaner.Make(Xmls.GetXmlStringWithNS());

            // Assert
            Assert.That(result,
                        Is.Not.Null &
                        Is.InstanceOf<XmlNamespacePrefixCleaner>()
            );
        }

        [TestCase(null)]
        [TestCase("")]
        [TestCase(" ")]
        [TestCase("   ")]
        public void Make__WhenXmlStringIsIncorrectString__ThrowArgumentNullException(string xmlstring)
        {
            // Arrange
            // Act
            // Assert
            Assert.That(() =>
                {
                    var result = XmlNamespacePrefixCleaner.Make(xmlstring);
                },
                Throws.ArgumentNullException
            );
        }

        #endregion

        #region XmlStringTransform Tests


        [Test()]
        public void XmlStringTransform__byDefault__XmlStringWithoutNamespacePrefixes()
        {
            // Arrange
            sut = CreateSUT(Xmls.GetXmlStringWithNS());

            // Act
            var result = sut.XmlStringTransform();

            // Assert
            Assert.That(result,
                            Does.Not.Match("<([a-zA-Z0-9]+):")
            );
        }


        [Test()]
        public void XmlStringTransform__WhenNamespacePrefixInInnerXmlDocument__XmlHasProperTag()
        {
            // Arrange
            sut = CreateSUT("<root><ns1:boot/></root>");

            // Act
            var result = sut.XmlStringTransform();

            // Assert
            Assert.That(result,
                            Does.Contain("<boot/>")
            );
        }

        [Test()]
        public void XmlStringTransform__WhenNamespacePrefixInInnerXmlDocument__XmlHasProperOpenAndCloseTags()
        {
            // Arrange
            sut = CreateSUT("<root><ns1:boot></ns1:boot></root>");

            // Act
            var result = sut.XmlStringTransform();

            // Assert
            Assert.That(result,
                            Does.Contain("<boot></boot>")
            );
        }

        [Test()]
        public void XmlStringTransform__WhenNamespacePrefixInHasUnderscore__XmlHasProperOpenAndCloseTags()
        {
            // Arrange
            sut = CreateSUT("<root><n_s1:boot></n_s1:boot></root>");

            // Act
            var result = sut.XmlStringTransform();

            // Assert
            Assert.That(result,
                            Does.Contain("<boot></boot>")
            );
        }


        [Test()]
        public void XmlStringTransform__WhenInnerTransformerIsNotNull__CallXmlStringTransformOnInnerTransformer()
        {
            // Arrange
            string xml = "<root><ns1:boot/></root>";
            transformer = Substitute.For<IXmlStringTransformer>();
            transformer.XmlStringTransform().Returns("<xml><ns1:item>some_text</ns1:item></xml>");
            sut = CreateSUT(xml, transformer);

            // Act
            var result = sut.XmlStringTransform();

            // Assert
            transformer.Received(1).XmlStringTransform();

        }

        [Test()]
        public void XmlStringTransform__WhenInnerTransformerIsNotNull__ResultHasTransformedString()
        {
            // Arrange
            string xml = "<root><ns1:boot/></root>";
            transformer = Substitute.For<IXmlStringTransformer>();
            transformer.XmlStringTransform().Returns("<xml><ns1:item>some_text</ns1:item></xml>");
            sut = CreateSUT(xml, transformer);

            // Act
            var result = sut.XmlStringTransform();

            // Assert

            Assert.That(result,
                            Does.Contain("<item>some_text</item>")
            );
        }



        #endregion


        #region help funcs

        private XmlNamespacePrefixCleaner CreateSUT(string xmlstring, IXmlStringTransformer transformer = null)
        {
            sut = XmlNamespacePrefixCleaner.Make(xmlstring, transformer);
            return sut;
        }



        #endregion
    }
}