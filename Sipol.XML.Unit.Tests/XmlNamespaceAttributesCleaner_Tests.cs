﻿using NSubstitute;

using NUnit.Framework;

using Sipol.XML.Interfaces;
using Sipol.XML.Unit.Tests;

using System;
using System.Text.RegularExpressions;

namespace Sipol.XML.Tests
{
    [TestFixture()]
    public class XmlNamespaceAttributesCleaner_Tests
    {
        private XmlNamespaceAttributesCleaner sut;
        private IXmlStringTransformer innerTransformer; 



        [SetUp]
        public void Init()
        {
            innerTransformer = null;
            sut = null;
        }

        #region XmlStringTransform Tests


        [Test()]
        public void XmlStringTransform__byDefault__ReturnXmlStringWithoutXmlnsAttributeWithPrefix()
        {
            // Arrange
            string xml = Xmls.GetXmlStringWithNS();
            sut = CreateSUT(xml, null);

            // Act
            var result = sut.XmlStringTransform();

            // Assert
            Assert.That(result,
                        Does.Not.Match(" xmlns:([a-zA-Z0-9]+)=")
            );
            Console.WriteLine(result);
        }


        [Test()]
        public void XmlStringTransform__WhenInnerTransformerIsNotNull__CallXmlStringTransformOnInnerTransformer()
        {
            // Arrange
            string xml = Xmls.GetXmlStringWithNS();
            innerTransformer = Substitute.For<IXmlStringTransformer>();
            innerTransformer.XmlStringTransform().Returns(Regex.Replace(xml, "<ns([0-9]+):", "<"));
            sut = CreateSUT(xml, innerTransformer);

            // Act
            var result = sut.XmlStringTransform();

            // Assert
            innerTransformer.Received().XmlStringTransform();

            Console.WriteLine(result);
        }


        [Test()]
        public void XmlStringTransform__byDefault__ReturnXmlStringWithoutXmlnsAttributeWithoutPrefix()
        {
            // Arrange
            string xml = Xmls.GetXmlStringWithNS();
            sut = CreateSUT(xml, null);

            // Act
            var result = sut.XmlStringTransform();

            // Assert
            Assert.That(result,
                        Does.Not.Match(" xmlns=")
            );

            Console.WriteLine(result);
        }

        [Test()]
        public void XmlStringTransform__byDefault__ReturnXmlStringWithoutNamespacePrefixes()
        {
            // Arrange
            string xml = Xmls.GetXmlStringWithNS();
            sut = CreateSUT(xml, null);

            // Act
            var result = sut.XmlStringTransform();

            // Assert
            Assert.That(result,
                        Does.Not.Match(" ([a-zA-Z0-9_]+):([a-zA-Z0-9_]+)=")
            );

            Console.WriteLine(result);

        }

        #endregion


        #region help funcs

        private XmlNamespaceAttributesCleaner CreateSUT(string xml, IXmlStringTransformer transformer = null)
        {
            sut = XmlNamespaceAttributesCleaner.Make(xml,transformer);
            return sut;
        }

        


        #endregion


    }
}