﻿using NUnit.Framework;
using Sipol.XML;
using Sipol.XML.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using NSubstitute;

namespace Sipol.XML.Tests
{
    [TestFixture()]
    public class XPathXmlElementMapper_Tests
    {
        private XPathXmlElementMapper sut = null;

        private XmlDocument doc = null;
        private IXPathXmlNodeFactory factory = null;


        #region ctor Tests
        [Test()]
        public void ctor()
        {

            object result = new XPathXmlElementMapper(new XmlDocument(), new XPathXmlNodeFactory());

            Assert.That(    result,

                            Is.Not.Null &
                            Is.InstanceOf<XPathXmlElementMapper>()
                );
        }

        [Test()]
        public void ctor_NullXmlDoc_ThrowException()
        {
            Assert.That(() => { object result = new XPathXmlElementMapper(null, new XPathXmlNodeShortFactory()); },

                        Throws.InstanceOf<ArgumentNullException>()
                );
        }

        [Test()]
        public void ctor_NullFactory_ThrowExcpetion()
        {

            Assert.That(() => { object result = new XPathXmlElementMapper(new XmlDocument(), null); },

                        Throws.InstanceOf<ArgumentNullException>()
                );
        }

        #endregion

        #region Count Tests
        [Test()]
        public void Count_AfterConstruct_ReturnZero()
        {
            CreateDefaultXmlDocument();
            CreateXPathXmlNodeShortFactory();
            sut = CreateSUT();

            Assert.That(sut.Count,
                        
                        Is.EqualTo(0)
                );
        }

        [Test()]
        public void Count_AfterCreateMap_GreaterThanZero()
        {
            CreateDefaultXmlDocument();
            CreateXPathXmlNodeShortFactory();
            sut = CreateSUT();
            sut.CreateMap();

            Assert.That(sut.Count,

                        Is.GreaterThan(0)
                );
        }

        #endregion


        #region CreateMap Tests
        [Test()]
        public void CreateMap_GetXPathFromXPathXmlNode_CallXPathInIXPathXmlNode()
        {
            CreateDefaultXmlDocument();
            factory = Substitute.For<IXPathXmlNodeFactory>();

            sut = CreateSUT();
            sut.CreateMap();

            factory.ReceivedWithAnyArgs().CreateXPathXmlNode(null);
        }


        [Test()]
        public void CreateMap_byDefault_MapExistsWith4ElementXPathShort()
        {
            CreateDefaultXmlDocument();
            CreateXPathXmlNodeShortFactory();

            sut = CreateSUT();
            sut.CreateMap();

            Assert.That(sut.Count,
                            Is.EqualTo(4)
                );

            Assert.That(sut.Get("/root/child1/child2"),
                      Is.Not.Null &
                      Is.InstanceOf<XmlElement>()
                );
            
        }
        #endregion

        #region Get Tests

        [Test()]
        public void Get_NoCreateMap_ThrowExcption()
        {
            CreateDefaultXmlDocument();
            CreateXPathXmlNodeShortFactory();
            sut = CreateSUT();

            Assert.That(            () => { XmlElement result = sut.Get("/abra/kana/bra"); },
                                    Throws.InstanceOf<Exception>().With.Message.EqualTo("No created Map")
                );
            
            
        }



        [Test()]
        public void Get_NotExistsXPath_ReturnNull()
        {
            CreateDefaultXmlDocument();
            CreateXPathXmlNodeShortFactory();
            sut = CreateSUT();
            sut.CreateMap();
            XmlElement result = sut.Get("/abra/kana/bra");
            Assert.That(result, Is.Null);
        }

        [Test()]

        public void Get_RootElement_OwnerDocumentIsDefaultDocument()
        {
            CreateDefaultXmlDocument();
            CreateXPathXmlNodeShortFactory();
            sut = CreateSUT();
            sut.CreateMap();

            XmlElement result = sut.Get("/root");
            Assert.That(result, Is.Not.Null);
            Assert.That(result.OwnerDocument, Is.Not.Null & Is.InstanceOf<XmlDocument>());
        }

        [Test()]
        public void Get_Child1ElementXPathAndRootElement_Child1HasSameRootElement()
        {
            CreateDefaultXmlDocument();
            CreateXPathXmlNodeShortFactory();
            sut = CreateSUT();
            sut.CreateMap();

            XmlElement root = sut.Get("/root");

            XmlElement result = sut.Get("/root/child1");
            Assert.That(result, Is.Not.Null);
            Assert.That(result.ParentNode, Is.Not.Null & Is.SameAs(root));
        }

        [Test()]
        public void Get_Child2ElementXPathShort_Child2FirstElementSelect()
        {
            CreateDefaultXmlDocument();
            CreateXPathXmlNodeShortFactory();
            sut = CreateSUT();
            sut.CreateMap();

            XmlElement result = sut.Get("/root/child1/child2");
            Assert.That (result, Is.Not.Null);
            Assert.That( result.InnerText, Is.Not.Null & Is.Not.Empty & Is.EqualTo("Hello2.1") );
        }

        [Test()]
        public void Get_byDefault_XmlElementWithDefaultDocument()
        {
            CreateDefaultXmlDocument();
            CreateXPathXmlNodeShortFactory();
            sut = CreateSUT();
            sut.CreateMap();

            XmlElement result = sut.Get("/root/child1/child2");
            Assert.That(result, Is.Not.Null);
            Assert.That(result.OwnerDocument, Is.Not.Null & Is.SameAs(doc));
        }
        #endregion

        #region Help funcs

        private XPathXmlElementMapper CreateSUT(XmlDocument doc, IXPathXmlNodeFactory xPathXmlNodeFactory)
        {
            sut = new XPathXmlElementMapper(doc, xPathXmlNodeFactory);
            return sut;
        }

        private XPathXmlElementMapper CreateSUT()
        {
            return CreateSUT(doc, factory);
        }

        private XmlDocument CreateDefaultXmlDocument(string xml)
        {
            doc = new XmlDocument();
            doc.LoadXml(xml);
            return doc;
        }

        private XmlDocument CreateDefaultXmlDocument()
        {
            return CreateDefaultXmlDocument(@"<root xmlns=""A"" xmlns:ns=""B"">
            <ns:child1>
                <child2>Hello2.1</child2>
                <child2>Hello2.2</child2>
            </ns:child1>
            <ns:child3/>
            </root>"
            );
        }

        private IXPathXmlNodeFactory CreateXPathXmlNodeShortFactory()
        {
            factory = new XPathXmlNodeShortFactory();
            return factory;
        }

        #endregion
    }
}