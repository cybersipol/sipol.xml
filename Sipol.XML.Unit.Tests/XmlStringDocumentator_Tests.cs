﻿using NSubstitute;
using NUnit.Framework;
using Sipol.XML.Interfaces;
using System.Xml;

namespace Sipol.XML.Tests
{
    [TestFixture()]
    public class XmlStringDocumentator_Tests
    {


        private XmlStringDocumentator sut;

        [SetUp]
        public void Init()
        {
            sut = null;
        }

        #region ctor Tests


        [Test()]
        public void ctor()
        {
            // Arrange

            // Act
            var result = new XmlStringDocumentator("<root/>");

            // Assert
            Assert.That(result,
                        Is.Not.Null &
                        Is.InstanceOf<XmlStringDocumentator>()
            );
        }

        [TestCase(null)]
        [TestCase(" ")]
        [TestCase("   ")]
        [TestCase("")]
        public void ctor__WhenXmlStringIsIncorrect__ThrowArgumentNullException(string xmlstring)
        {
            // Arrange

            // Act

            // Assert
            Assert.That(() =>
                {
                    var result = new XmlStringDocumentator(xmlstring);
                },
                Throws.ArgumentNullException
            );
        }

        #endregion


        #region XmlDocument Tests


        [Test()]
        public void XmlDocument__byDefault__InstanceOfXmlDocument()
        {
            // Arrange
            sut = CreateSUT("<root/>");
            // Act
            var result = sut.XmlDocument;

            // Assert
            Assert.That(result,
                        Is.Not.Null &
                        Is.InstanceOf<XmlDocument>()
            );
        }

        [Test()]
        public void XmlDocument__WhenXmlIsNotXmlString__ThrowExceptionWithMessageAndInnerException()
        {
            // Arrange
            sut = CreateSUT("some text");

            // Act
            // Assert
            Assert.That(() =>
                {
                    var result = sut.XmlDocument;
                },
                Throws.Exception.With.Message.Contain("Cannot load Xml document").And.With.InnerException.Not.Null
            );
        }

        [Test()]
        public void XmlDocument__byDefault__CallXmlStringTransformOnXmlStringTrafsormer()
        {
            // Arrange
            sut = CreateSUT("<root/>");
            sut.XmlStringTransformer = Substitute.For<IXmlStringTransformer>();
            sut.XmlStringTransformer.XmlStringTransform().Returns("<boot />");

            // Act
            var result = sut.XmlDocument;

            // Assert
            sut.XmlStringTransformer.Received().XmlStringTransform();
        }

        [Test()]
        public void XmlDocument__byDefault__InstanceOfXmlDocumetWithTransformedXml()
        {
            // Arrange
            sut = CreateSUT("<root/>");
            sut.XmlStringTransformer = Substitute.For<IXmlStringTransformer>();
            sut.XmlStringTransformer.XmlStringTransform().Returns("<boot />");

            // Act
            var result = sut.XmlDocument;

            // Assert
            Assert.That(result,
                            Is.Not.Null &
                            Is.InstanceOf<XmlDocument>()

            );

            Assert.That(result.DocumentElement.LocalName,
                            Is.EqualTo("boot")
                            
            );
        }

        [Test()]
        public void XmlDocument__WhenTwoGetXmlDocument__TwoXmlDocumentIsSame()
        {
            // Arrange
            sut = CreateSUT("<root/>");
            // Act
            var result1 = sut.XmlDocument;
            var result2 = sut.XmlDocument;

            // Assert
            Assert.That(result1,
                        Is.Not.Null &
                        Is.SameAs(result2)
            );
        }


        [TestCase("bla blo blue")]
        [TestCase("</>")]
        [TestCase("()[<gaga>]")]
        public void XmlDocument__WhenXmlStringIsIncorrect__ThrowXmlException(string incorrectXmlString)
        {
            // Arrange
            sut = CreateSUT(incorrectXmlString);

            // Act
            // Assert
            Assert.That(() =>
                {
                    var result = sut.XmlDocument;
                },
                Throws.Exception.With.InnerException.InstanceOf<XmlException>()
            );
        }

        #endregion


        #region XmlStringTransform Tests


        [Test()]
        public void XmlStringTransform__byDefault__CallXmlStringTransformOnSutXmlStringTransformer()
        {
            // Arrange
            sut = CreateSUT("<root />");
            sut.XmlStringTransformer = Substitute.For<IXmlStringTransformer>();

            // Act
            var result = sut.XmlStringTransform();


            // Assert
            sut.XmlStringTransformer.Received(1).XmlStringTransform();
        }

        [Test()]
        public void XmlStringTransform__byDefault__ReturnXmlFromCallingXmlStringTransformOnXmlStringTransformer()
        {
            // Arrange
            string xmlStart = "<root />";
            string xmlTransform = "<boot>" + xmlStart + "</boot>";
            sut = CreateSUT(xmlStart);
            sut.XmlStringTransformer = Substitute.For<IXmlStringTransformer>();
            sut.XmlStringTransformer.XmlStringTransform().Returns(xmlTransform);

            // Act
            var result = sut.XmlStringTransform();

            // Assert
            Assert.That(result,
                            Is.EqualTo(xmlTransform)
            );
        }

        [Test()]
        public void XmlStringTransform__WhenXmlStringTransformerIsNull__ReturnXmlEqualToCreatedXml()
        {
            // Arrange
            string xml = "<root />";
            sut = CreateSUT(xml);
            sut.XmlStringTransformer = null;

            // Act
            var result = sut.XmlStringTransform();


            // Assert
            Assert.That(result,
                          Is.EqualTo(xml)  
            );
        }

        #endregion


        #region help funcs

        private XmlStringDocumentator CreateSUT(string xmlstring)
        {
            sut = new XmlStringDocumentator(xmlstring);
            return sut;
        }



        #endregion

    }
}