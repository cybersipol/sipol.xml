﻿using NUnit.Framework;
using Sipol.XML;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sipol.XML.Tests
{
    [TestFixture()]
    public class XPathMainTagReader_Tests
    {

        private XPathMainTagReader sut;

        [SetUp]
        public void Init()
        {
            sut = null;
        }

        #region GetMainTag Tests


        [TestCase("/root/aaaa","root")]
        [TestCase("/ns:root/aaaa", "ns:root")]
        [TestCase("////root/aaaa", "root")]
        public void GetMainTag__byDefault__ReturnMainTag(string xpath, string expectedMaintag)
        {
            // Arrange
            sut = CreateSUT(xpath);
            // Act
            string result = sut.GetMainTag();

            // Assert
            Assert.That(result,
                        Is.EqualTo(expectedMaintag)
            );
        }

        [TestCase(null)]
        [TestCase("")]
        [TestCase(" ")]
        [TestCase("   ")]
        [TestCase("aajajja...akaakk")]
        public void GetMainTag__WhenXPathIsIncorrect__EmptyResult(string xpath)
        {
            // Arrange
            sut = CreateSUT(xpath);
            // Act
            string result = sut.GetMainTag();

            // Assert
            Assert.That(result,
                        Is.Empty
            );
        }

        #endregion


        #region help funcs
        private XPathMainTagReader CreateSUT( string xpath )
        {
            sut = new XPathMainTagReader(xpath);
            return sut;
        }



        #endregion
    }
}