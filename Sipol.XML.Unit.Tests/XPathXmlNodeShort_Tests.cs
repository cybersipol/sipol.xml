﻿using InfoVeriti.Net.Extensions.Xml;
using NUnit.Framework;

using System.Xml;

namespace Sipol.XML.Tests
{
    [TestFixture()]
    public class XPathXmlNodeShort_Tests
    {
        [Test()]
        public void Path_NotUseIndexerAndUseLocalNames_PathWithoutIndexAndWithoutNamespacePrefix()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(@"<root xmlns=""A"" xmlns:ns=""B""><ns:child1><child2>Hello</child2><child2>Hello2</child2></ns:child1><ns:child3/></root>");

            XmlElement root = doc.DocumentElement;

            doc.IterateThroughAllNodes(delegate (XmlNode node)
            {
                if (node.LocalName.Equals("child2"))
                {
                    XPathXmlNode path = new XPathXmlNodeShort(node);

                    Assert.That(path.XPath, Is.EqualTo("/root/child1/child2"));
                }
            });

        }
    }
}