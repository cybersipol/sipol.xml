﻿using NUnit.Framework;
using Sipol.XML;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Sipol.XML.Tests
{
    [TestFixture()]
    public class XPathXmlNodeFactory_Tests
    {
        private XPathXmlNodeFactory sut = null;

        [Test()]
        public void CreateXPathXmlNode_NullNode_ThrowExcption()
        {
            sut = new XPathXmlNodeFactory();
            Assert.That(() => { sut.CreateXPathXmlNode(null); },
                    Throws.Exception
                );
        }

        [Test()]
        public void CreateXPathXmlNode_byDefault_ReturnNotNullWithProperType()
        {
            sut = new XPathXmlNodeFactory();
            XmlDocument doc = new XmlDocument();
            XmlElement elem = doc.CreateElement("root");

            object result = sut.CreateXPathXmlNode(elem);
            Assert.That(result,

                        Is.Not.Null &
                        Is.InstanceOf<XPathXmlNode>()
                );
        }
    }
}