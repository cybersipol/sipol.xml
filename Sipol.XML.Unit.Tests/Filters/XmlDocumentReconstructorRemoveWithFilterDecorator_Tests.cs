﻿using NUnit.Framework;
using Sipol.XML.Filters.Interfaces;
using System.Collections.Generic;
using System.Xml;
using NSubstitute;

namespace Sipol.XML.Filters.Tests
{
    [TestFixture()]
    public class XmlDocumentReconstructorRemoveWithFilterDecorator_Tests
    {
        private SutMock sut = null;
        private IXmlNodeFilter filter = null;


        [SetUp]
        public void Init()
        {
            sut = null;
            filter = null;
        }




        #region ctor Tests


        [Test()]
        public void ctor_NullXmlDocument_ThrowException()
        {
            Assert.That(() =>
                        {
                            object result = new XmlDocumentReconstructorRemoveWithFilter(null, null);
                        },
                        Throws.ArgumentNullException
            );
        }


        [Test()]
        public void ctor()
        {

            object result = new XmlDocumentReconstructorRemoveWithFilter(new XmlDocument(), null);

            Assert.That(result,
                        Is.Not.Null &
                        Is.InstanceOf<XmlDocumentReconstructorRemoveWithFilter>()
                        );
        }


        #endregion




        #region IsNodeToReconstruct Tests


        [Test()]
        public void IsNodeToReconstruct_NullNode_ReturnFalse()
        {
            XmlDocument doc = CreateXmlDocument("<root />");

            sut = CreateSUT(doc, null);

            bool result = sut.Call_IsNodeToReconstruct(null);

            Assert.That(result,
                        Is.EqualTo(false)
                        );
        }


        [Test()]
        public void IsNodeToReconstruct_NotNullNodeAndFilterIsNull_ReturnFalse()
        {
            XmlDocument doc = CreateXmlDocument("<root />");

            sut = CreateSUT(doc, null);

            bool result = sut.Call_IsNodeToReconstruct(doc.DocumentElement);

            Assert.That(result,
                        Is.EqualTo(false)
                        );
        }


        [Test()]
        public void IsNodeToReconstruct_NullNodeAndFilterIsNotNull_ReturnFalse()
        {
            XmlDocument doc = CreateXmlDocument("<root />");

            filter = CreateFilterMock();

            sut = CreateSUT(doc, filter);


            bool result = sut.Call_IsNodeToReconstruct(null);


            Assert.That(result,
                        Is.EqualTo(false)
                        );
        }



        [Test()]
        public void IsNodeToReconstruct_NotNullNodeAndFilterIsNotNull_CallFilterIsXmlNodeAccepted()
        {
            XmlDocument doc = CreateXmlDocument("<root />");

            filter = CreateFilterMock();

            sut = CreateSUT(doc, filter);


        
            sut.Call_IsNodeToReconstruct(doc.DocumentElement);



            filter.Received(1).IsXmlNodeAccepted(doc.DocumentElement);
        }


        [TestCase(true)]
        [TestCase(false)]
        public void IsNodeToReconstruct_NotNullNodeAndFilterIsNotNull_ReturnFilterIsXmlNodeAccepted(bool IsXmlNodeAccpetedReturn)
        {
            XmlDocument doc = CreateXmlDocument("<root />");

            filter = CreateFilterMock();
            filter.IsXmlNodeAccepted(doc.DocumentElement).Returns(IsXmlNodeAccpetedReturn);

            sut = CreateSUT(doc, filter);

            bool result = sut.Call_IsNodeToReconstruct(doc.DocumentElement);

            Assert.That(result,
                        Is.EqualTo(IsXmlNodeAccpetedReturn)
                        );
        }

        #endregion




        #region ReconstructWithNodes Tests


        [Test]
        public void ReconstructWithNodes_RemoveChild2_XmlDomWithoutChild2()
        {
            string xml = "<root><child2>test1</child2><child1 /><child2>test2</child2><child2>test3</child2></root>";
            XmlDocument doc = CreateXmlDocument(xml);

            sut = CreateSUT(doc, null);

            List<XmlNode> lstNodes = new List<XmlNode>();
            lstNodes.Add(doc.DocumentElement.ChildNodes[0]);
            lstNodes.Add(doc.DocumentElement.ChildNodes[2]);
            lstNodes.Add(doc.DocumentElement.ChildNodes[3]);


            sut.Call_ReconstructWithNodes(lstNodes);


            Assert.That(doc.InnerXml,
                            Is.EqualTo("<root><child1 /></root>")
                        );

        }


        [Test]
        public void ReconstructWithNodes_NullListNodes_XmlDomWithoutChanges()
        {
            string xml = "<root><child2>test1</child2><child1 /><child2>test2</child2><child2>test3</child2></root>";
            XmlDocument doc = CreateXmlDocument(xml);

            sut = CreateSUT(doc, null);

            sut.Call_ReconstructWithNodes(null);

            Assert.That(doc.InnerXml,
                            Is.EqualTo(xml)
                        );

        }


        [Test]
        public void ReconstructWithNodes_EmptyListNodes_XmlDomWithoutChanges()
        {
            string xml = "<root><child2>test1</child2><child1 /><child2>test2</child2><child2>test3</child2></root>";
            XmlDocument doc = CreateXmlDocument(xml);

            sut = CreateSUT(doc, null);

            List<XmlNode> lstNodes = new List<XmlNode>();

            sut.Call_ReconstructWithNodes(lstNodes);

            Assert.That(doc.InnerXml,
                            Is.EqualTo(xml)
                        );

        }


        #endregion


        #region Help funcs


        private SutMock CreateSUT( XmlDocument doc, IXmlNodeFilter nodeFilter = null)
        {
            return new SutMock(doc, nodeFilter);
        }


        private XmlDocument CreateXmlDocument( string xml ) => XmlDocumentReconstructor_Tests.CreateXmlDocument(xml);


        private IXmlNodeFilter CreateFilterMock()
        {
            filter = Substitute.For<IXmlNodeFilter>();
            return filter;
        }


        #endregion



        #region Help class


        private class SutMock : XmlDocumentReconstructorRemoveWithFilter
        {
            public SutMock( XmlDocument doc, IXmlNodeFilter filter = null ) : base(doc, filter)
            {
            }

            public bool Call_IsNodeToReconstruct( XmlNode node ) => IsNodeToReconstruct(node);
            public void Call_ReconstructWithNodes( IList<XmlNode> nodes ) => ReconstructWithNodes(nodes);

            protected override bool IsNodeToReconstruct( XmlNode node )
            {
                return base.IsNodeToReconstruct(node);
            }

            protected override void ReconstructWithNodes( IList<XmlNode> nodes )
            {
                base.ReconstructWithNodes(nodes);
            }
        }


        #endregion

    }
}