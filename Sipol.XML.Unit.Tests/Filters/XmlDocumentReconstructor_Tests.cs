﻿using InfoVeriti.Net.Extensions.Xml;
using NUnit.Framework;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;

namespace Sipol.XML.Filters.Tests
{
    [TestFixture()]
    public class XmlDocumentReconstructor_Tests
    {
        private SutMock sut = null;

        [SetUp]
        public void Init()
        {
            sut = null;
        }


        #region ctor Tests

        [Test]
        public void ctor_NullXmlDocument_ThrowException()
        {
            Assert.That(() =>
            {
                object result = new SutMock(null);
            },
            Throws.ArgumentNullException
            );
        }


        [Test]
        public void ctor()
        {
            object result = new SutMock(new XmlDocument());

            Assert.That(result,
                        Is.Not.Null &
                        Is.InstanceOf<XmlDocumentReconstructor>()
                        );
        }

        #endregion


        #region RecontructXmlDocument Tests

        [Test()]
        public void RecontructXmlDocument_CheckCallIsNodeToReconstruct_CallOnEachXmlDoneInXmlDocument()
        {
            XmlDocument doc = CreateXmlDocument("<root><child1/><child2>test</child2></root>");

            sut = new SutMock(doc);
            sut.countCall_IsNodeToReconstruct = 0;


            sut.RecontructXmlDocument();


            Assert.That(sut.countCall_IsNodeToReconstruct,
                        Is.EqualTo(4)
                        );
        }


        [Test()]
        public void RecontructXmlDocument_CheckCallReconstructWithNodes_OneCallReconstructWithNodes()
        {
            XmlDocument doc = CreateXmlDocument("<root><child1/><child2>test</child2></root>");

            sut = new SutMock(doc);
            sut.countCall_ReconstructWithNodes = 0;
            sut.DefaultIsNodeReconstruct = true;

            sut.RecontructXmlDocument();


            Assert.That(sut.countCall_ReconstructWithNodes,
                        Is.EqualTo(1)
                        );
        }

        [Test()]
        public void RecontructXmlDocument_CheckCallReconstructWithNodesWhenNoNodes_NoCallReconstructWithNodes()
        {
            XmlDocument doc = CreateXmlDocument("<root><child1/><child2>test</child2></root>");

            sut = new SutMock(doc);
            sut.countCall_ReconstructWithNodes = 0;
            sut.DefaultIsNodeReconstruct = false;

            sut.RecontructXmlDocument();


            Assert.That(sut.countCall_ReconstructWithNodes,
                        Is.EqualTo(0)
                        );
        }


        [Test()]
        public void RecontructXmlDocument_ReconstructAllNodes_nodesToReconstructContainsAllNodes()
        {
            XmlDocument doc = CreateXmlDocument("<root><child1/><child2>test</child2></root>");

            List<XmlNode> xmlAllNodes = new List<XmlNode>();
            XmlDocumentExt.IterateThroughAllNodes(doc, delegate (XmlNode node)
            {
                if (node != null)
                    xmlAllNodes.Add(node);
            });

            sut = new SutMock(doc);
            sut.countCall_ReconstructWithNodes = 0;

            sut.DefaultIsNodeReconstruct = true;
            sut.nodesToReconstruct = null;


            sut.RecontructXmlDocument();


            Assert.That(sut.nodesToReconstruct,
                                    Is.Not.Null &
                                    Is.EquivalentTo(xmlAllNodes) &
                                    Is.Not.SameAs(xmlAllNodes)
                        );
        }


        [Test()]
        public void RecontructXmlDocument_ReconstructChildNodes_nodesToReconstructContainsOlnyChildNode()
        {
            XmlDocument doc = CreateXmlDocument("<root><child1/><child2>test</child2></root>");

            List<XmlNode> xmlChildNodes = new List<XmlNode>();
            XmlDocumentExt.IterateThroughAllNodes(doc, delegate (XmlNode node)
            {
                if (node != null && node.LocalName.StartsWith("child"))
                    xmlChildNodes.Add(node);
            });

            sut = new SutMock(doc);
            sut.countCall_ReconstructWithNodes = 0;
            sut.OnNodeCheck += delegate (XmlNode node) { return node.LocalName.StartsWith("child"); };
            sut.DefaultIsNodeReconstruct = false;
            sut.nodesToReconstruct = null;

            sut.RecontructXmlDocument();

            Assert.That(sut.nodesToReconstruct,
                                    Is.Not.Null &
                                    Is.EquivalentTo(xmlChildNodes) &
                                    Is.Not.SameAs(xmlChildNodes)
                        );
        }


        [Test()]
        public void RecontructXmlDocument_ReconstructChildNodes_nodesToReconstructSortedByLevelDesc()
        {
            XmlDocument doc = CreateXmlDocument("<root><child1/><child2><child3>test</child3></child2><child4><child5/></child4></root>");

            List<XmlNode> xmlChildNodes = new List<XmlNode>();
            XmlDocumentExt.IterateThroughAllNodes(doc, delegate (XmlNode node)
            {
                if (node != null && node.LocalName.StartsWith("child"))
                    xmlChildNodes.Add(node);
            });

            List<XmlNode> sortedXmlChildNodes = (from n in xmlChildNodes
                                                 orderby doc.GetNodeLevel(n) descending, n.Name
                                                 select n).ToList();

            sut = new SutMock(doc);
            sut.countCall_ReconstructWithNodes = 0;
            sut.OnNodeCheck += delegate (XmlNode node) { return node.LocalName.StartsWith("child"); };
            sut.DefaultIsNodeReconstruct = false;
            sut.nodesToReconstruct = null;

            sut.RecontructXmlDocument();

            Assert.That(sut.nodesToReconstruct,
                                    Is.Not.Null &
                                    Is.EqualTo(sortedXmlChildNodes) &
                                    Is.Not.SameAs(sortedXmlChildNodes)
                        );
        }

        //[Test()]
        public void RecontructXmlDocument_Scenario_Expected()
        {
            Assert.Fail("Finish test");
        }

        #endregion


        #region RemoveNodes Tests

        [Test()]
        public void RemoveNodes_RemoveAllChildElement_OlnyRootElementWillBe()
        {
            XmlDocument doc = CreateXmlDocument("<root><child1/><child2>test</child2></root>");

            List<XmlNode> xmlAllNodesWithoutRoot = new List<XmlNode>();
            doc.IterateThroughAllNodes(delegate (XmlNode node)
            {
                if (node != null && !node.LocalName.Equals("root"))
                    xmlAllNodesWithoutRoot.Add(node);
            });

            sut = new SutMock(doc);


            sut.RemoveNodes(xmlAllNodesWithoutRoot);


            Assert.That(doc.InnerXml,
                            Is.EqualTo("<root></root>") |
                            Is.EqualTo("<root/>")
                        );

        }


        [Test()]
        public void RemoveNodes_RemoveSecondChildNode_ValidXmlWillBeWithoutSecondChildNode()
        {
            XmlDocument doc = CreateXmlDocument("<root><child2>test1</child2><child1/><child2>test2</child2><child2>test3</child2></root>");

            List<XmlNode> xmlSecondChildNodes = new List<XmlNode>();
            doc.IterateThroughAllNodes(delegate (XmlNode node)
                {
                    if (node != null && node.LocalName.Equals("child2"))
                        xmlSecondChildNodes.Add(node);
                }
            );

            sut = new SutMock(doc);


            sut.RemoveNodes(xmlSecondChildNodes);


            Assert.That(doc.InnerXml,
                            Is.EqualTo("<root><child1 /></root>")
                        );
        }


        [Test()]
        public void RemoveNodes_RemoveRootElement_EmptyXmlDocument()
        {
            XmlDocument doc = CreateXmlDocument("<root><child2>test1</child2><child1/><child2>test2</child2><child2>test3</child2></root>");

            List<XmlNode> xmlRootNodes = new List<XmlNode>();
            doc.IterateThroughAllNodes(delegate (XmlNode node)
            {
                if (node != null && node.LocalName.Equals("root"))
                    xmlRootNodes.Add(node);
            }
            );

            sut = new SutMock(doc);

            sut.RemoveNodes(xmlRootNodes);

            Assert.That(doc.InnerXml,
                            Is.EqualTo(String.Empty)
                        );
        }


        [Test()]
        public void RemoveNodes_NullListOfNodes_XmlDocumentNotChange()
        {
            string xml = "<root><child2>test1</child2><child1 /><child2>test2</child2><child2>test3</child2></root>";
            XmlDocument doc = CreateXmlDocument(xml);

            sut = new SutMock(doc);

            sut.RemoveNodes(null);

            Assert.That(doc.InnerXml,
                            Is.EqualTo(xml)
                        );
        }

        #endregion


        #region Mock class

        private class SutMock : XmlDocumentReconstructor
        {
            public delegate bool NodeCheck(XmlNode node);

            public event NodeCheck OnNodeCheck;

            public bool DefaultIsNodeReconstruct { get; set; } = false;
            public IList<XmlNode> nodesToReconstruct { get; set; } = null;

            public int countCall_IsNodeToReconstruct { get; set; } = 0;
            public int countCall_ReconstructWithNodes { get; set; } = 0;
            public bool IsConstructCall { get; private set; } = false;

            public SutMock(XmlDocument xmlDoc) : base(xmlDoc)
            {
                IsConstructCall = true;
            }

            protected override bool IsNodeToReconstruct(XmlNode node)
            {
                countCall_IsNodeToReconstruct++;
                return OnNodeCheck?.Invoke(node) ?? DefaultIsNodeReconstruct;
            }

            protected override void ReconstructWithNodes(IList<XmlNode> nodes)
            {
                countCall_ReconstructWithNodes++;
                nodesToReconstruct = nodes;
            }
        }

        #endregion


        #region Help funcs

        internal static XmlDocument CreateXmlDocument(string xml)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);

            return doc;
        }

        private XmlDocument CreateDefaultXmlDocument()
        {
            return CreateXmlDocument("<root><child1/><child2>test</child2></root>");
        }

        #endregion
    }
}