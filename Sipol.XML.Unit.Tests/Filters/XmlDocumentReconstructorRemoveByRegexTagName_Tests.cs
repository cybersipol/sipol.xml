﻿using NUnit.Framework;
using Sipol.XML.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Sipol.XML.Filters.Tests
{
    [TestFixture()]
    public class XmlDocumentReconstructorRemoveByRegexTagName_Tests
    {
        private SutMock sut = null;


        [SetUp]
        public void Init()
        {
            sut = null;
        }



        #region ctor Tests

        [Test()]
        public void ctor()
        {
            object result = new XmlDocumentReconstructorRemoveByRegexTagName(new XmlDocument(), null, true);

            Assert.That(result,
                        Is.Not.Null &
                        Is.InstanceOf<XmlDocumentReconstructorRemoveByRegexTagName>()
                        );
        }



        [Test()]
        public void ctor_NullXmlDocument_ThrowException()
        {
            Assert.That(() => 
            {
                object result = new XmlDocumentReconstructorRemoveByRegexTagName(null, null, true);
            },
            Throws.ArgumentNullException
            );
        }


        #endregion

        #region IsNodeToReconstruct Tests


        [TestCase("root", "^ch(.+)d([0-9]+)$", false)]
        [TestCase("root", "^r([o]{2})t$", true)]
        [TestCase("child1", "^child1$", true)]
        [TestCase("child1", "^child([0-9]+)$", true)]
        [TestCase("child2", null, false)]
        [TestCase("child3", "", false)]
        [TestCase("child3", "  ", false)]
        public void IsNodeToReconstruct_RegexTagName_ExpectedResultForDocGetElementsByTagName( string nodeName, string regexTagName, bool expectedResult )
        {
            string xml = "<root><child2>test1</child2><child1 /><child2>test2</child2><child2>test3</child2><child_4>aaa</child_4><child1><child3 /></child1></root>";
            XmlDocument doc = CreateXmlDocument(xml);

            XmlNodeList lst = doc.GetElementsByTagName(nodeName);

            if (lst.Count <= 0)
                Assert.Fail("No nodes in XmlDocument with tagname " + nodeName);

            sut = CreateSUT(doc, regexTagName, true);

            foreach (XmlNode node in lst)
            {
                bool result = sut.Call_IsNodeToReconstruct(node);
                Assert.That(result,
                            Is.EqualTo(expectedResult)
                            );
            }


        }


        [TestCase("root", "^ch(.+)d([0-9]+)$", false)]
        [TestCase("root", "^r([o]{2})t$", true)]
        [TestCase("child1", "^child1$", true)]
        [TestCase("child1", "^child([0-9]+)$", true)]
        [TestCase("child2", null, false)]
        [TestCase("a:child3", "", false)]
        [TestCase("a:child3", "      ", false)]
        [TestCase("a:child3", "^child([0-9]+)", false)]
        [TestCase("a:child3", "a\\:child([0-9]+)", true)]
        [TestCase("a:child_4", "^child_([0-9]+)", false)]
        [TestCase("a:child_4", "^a\\:child_([0-9]+)", true)]
        public void IsNodeToReconstruct_RegexTagName_ExpectedResultForDocGetElementsByTagNameWithNamespace( string nodeName, string regexTagName, bool expectedResult )
        {
            string xml = @"<root xmlns:a=""a""><child2>test1</child2><child1 /><child2>test2</child2><child2>test3</child2><a:child_4>aaa</a:child_4><child1><a:child3 /></child1></root>";
            XmlDocument doc = CreateXmlDocument(xml);

            XmlNodeList lst = doc.GetElementsByTagName(nodeName);

            if (lst.Count <= 0)
                Assert.Fail("No nodes in XmlDocument with tagname " + nodeName);

            sut = CreateSUT(doc, regexTagName, false);

            foreach (XmlNode node in lst)
            {
                bool result = sut.Call_IsNodeToReconstruct(node);
                Assert.That(result,
                            Is.EqualTo(expectedResult)
                            );
            }


        }


        [Test]
        public void IsNodeToReconstruct_NullNode_ExpectedFalse()
        {
            string xml = "<root><child2>test1</child2><child1 /><child2>test2</child2><child2>test3</child2><child_4>aaa</child_4><child1><child3 /></child1></root>";
            XmlDocument doc = CreateXmlDocument(xml);

            sut = CreateSUT(doc, "a(.+)$");

            bool result = sut.Call_IsNodeToReconstruct(null);
            Assert.That(result,
                        Is.EqualTo(false)
                       );

        }

        #endregion


        #region ReconstructWithNodes Tests

        [Test]
        public void ReconstructWithNodes_RemoveChild2_XmlDomWithoutChild2()
        {
            string xml = "<root><child2>test1</child2><child1 /><child2>test2</child2><child2>test3</child2></root>";
            XmlDocument doc = CreateXmlDocument(xml);

            string tagName = "child2";
            sut = CreateSUT(doc, tagName);

            List<XmlNode> lstNodes = new List<XmlNode>();
            lstNodes.Add(doc.DocumentElement.ChildNodes[0]);
            lstNodes.Add(doc.DocumentElement.ChildNodes[2]);
            lstNodes.Add(doc.DocumentElement.ChildNodes[3]);

            sut.Call_ReconstructWithNodes(lstNodes);

            Assert.That(doc.InnerXml,
                            Is.EqualTo("<root><child1 /></root>")
                        );

        }


        [Test]
        public void ReconstructWithNodes_NullListNodes_XmlDomWithoutChanges()
        {
            string xml = "<root><child2>test1</child2><child1 /><child2>test2</child2><child2>test3</child2></root>";
            XmlDocument doc = CreateXmlDocument(xml);

            string tagName = "child2";
            sut = CreateSUT(doc, tagName);

            sut.Call_ReconstructWithNodes(null);

            Assert.That(doc.InnerXml,
                            Is.EqualTo(xml)
                        );

        }


        [Test]
        public void ReconstructWithNodes_EmptyListNodes_XmlDomWithoutChanges()
        {
            string xml = "<root><child2>test1</child2><child1 /><child2>test2</child2><child2>test3</child2></root>";
            XmlDocument doc = CreateXmlDocument(xml);

            string tagName = "child2";
            sut = CreateSUT(doc, tagName);

            List<XmlNode> lstNodes = new List<XmlNode>();

            sut.Call_ReconstructWithNodes(lstNodes);

            Assert.That(doc.InnerXml,
                            Is.EqualTo(xml)
                        );

        }


        #endregion

        #region Help funcs

        private SutMock CreateSUT(XmlDocument doc, string tagName = null, bool isLocalName = true)
        {
            return new SutMock(doc, tagName, isLocalName);
        }

        private XmlDocument CreateXmlDocument(string xml) => XmlDocumentReconstructor_Tests.CreateXmlDocument(xml);

        #endregion


        #region Help class

        private class SutMock : XmlDocumentReconstructorRemoveByRegexTagName
        {
            public SutMock( XmlDocument xmlDoc, string regexTagName = null, bool isLocalName = true ) : base(xmlDoc, regexTagName, isLocalName)
            {
            }

            public bool Call_IsNodeToReconstruct(XmlNode node) => IsNodeToReconstruct(node);
            public void Call_ReconstructWithNodes(IList<XmlNode> nodes) => ReconstructWithNodes(nodes);

            protected override bool IsNodeToReconstruct(XmlNode node)
            {
                return base.IsNodeToReconstruct(node);
            }

            protected override void ReconstructWithNodes(IList<XmlNode> nodes)
            {
                base.ReconstructWithNodes(nodes);
            }
        }

        #endregion
    }
}