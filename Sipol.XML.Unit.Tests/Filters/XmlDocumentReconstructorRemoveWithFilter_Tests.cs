﻿using NUnit.Framework;
using Sipol.XML.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NSubstitute;
using System.Xml;

namespace Sipol.XML.Filters.Tests
{
    [TestFixture()]
    public class XmlDocumentReconstructorRemoveWithFilter_Tests
    {
        private SutMock sut = null;


        [SetUp]
        public void Init()
        {
            sut = null;
        }

        #region ctor Tests

        [Test()]
        public void ctor()
        {
            object result = new XmlDocumentReconstructorRemoveByTagName(new XmlDocument());

            Assert.That(result,
                            Is.Not.Null &
                            Is.InstanceOf<XmlDocumentReconstructorRemoveByTagName>()
                        );
        }


        [Test()]
        public void ctor_NullXmlDocument_ThrowException()
        {
            Assert.That(() =>
                        {
                            object result = new XmlDocumentReconstructorRemoveByTagName(null);
                        },
                        Throws.ArgumentNullException
            );

        }

        #endregion

        #region IsNodeToReconstruct Tests

        [TestCase("root", false)]
        [TestCase("child1", false)]
        [TestCase("child2", true)]
        [TestCase("child3", false)]
        public void IsNodeToReconstruct_TagName_ExpectedResultForChild2(string tagName, bool expectedResult)
        {
            string xml = "<root><child2>test1</child2><child1 /><child2>test2</child2><child2>test3</child2></root>";
            XmlDocument doc = CreateXmlDocument(xml);

            sut = CreateSUT(doc, tagName);

            bool result = sut.Call_IsNodeToReconstruct(doc.DocumentElement.ChildNodes[2]);

            Assert.That(result,
                        Is.EqualTo(expectedResult)
                        );
        }




        [TestCase(null)]
        [TestCase("")]
        [TestCase("   ")]
        public void IsNodeToReconstruct_TagNameEmpty_False(string tagName)
        {
            string xml = "<root><child2>test1</child2><child1 /><child2>test2</child2><child2>test3</child2></root>";
            XmlDocument doc = CreateXmlDocument(xml);

            sut = CreateSUT(doc, tagName);

            bool result = sut.Call_IsNodeToReconstruct(doc.DocumentElement.ChildNodes[0]);

            Assert.That(result,
                        Is.False);
        }



        [TestCase("root", false)]
        [TestCase("child1", false)]
        [TestCase("a:child1", true)]
        [TestCase("b:child1", false)]
        [TestCase("child2", false)]
        [TestCase("child3", false)]
        public void IsNodeToReconstruct_TagNameWithNamespace_ExpectedResultForChild1WithNamespace(string tagName, bool expectedResult)
        {
            string xml = @"<root xmlns:a=""a""><child2>test1</child2><a:child1 /><child2>test2</child2><child2>test3</child2><a:child1>aaa</a:child1></root>";
            XmlDocument doc = CreateXmlDocument(xml);

            sut = CreateSUT(doc, tagName, false);

            bool result = sut.Call_IsNodeToReconstruct(doc.DocumentElement.ChildNodes[1]);

            Assert.That(result,
                        Is.EqualTo(expectedResult)
                        );
        }


        [Test]
        public void IsNodeToReconstruct_NodeFromOutsideDocument_False()
        {
            string xml = @"<root xmlns:a=""a""><child2>test1</child2><a:child1 /><child2>test2</child2><child2>test3</child2><a:child1>aaa</a:child1></root>";
            XmlDocument doc = CreateXmlDocument(xml);

            XmlDocument doc2 = CreateXmlDocument(xml);

            sut = CreateSUT(doc, "child2", true);

            bool result = sut.Call_IsNodeToReconstruct(doc2.DocumentElement.ChildNodes[0]);

            Assert.That(result,
                        Is.False
                        );
        }


        #endregion


        #region ReconstructWithNodes Tests

        [Test]
        public void ReconstructWithNodes_RemoveChild2_XmlDomWithoutChild2()
        {
            string xml = "<root><child2>test1</child2><child1 /><child2>test2</child2><child2>test3</child2></root>";
            XmlDocument doc = CreateXmlDocument(xml);

            string tagName = "child2";
            sut = CreateSUT(doc, tagName);

            List<XmlNode> lstNodes = new List<XmlNode>();
            lstNodes.Add(doc.DocumentElement.ChildNodes[0]);
            lstNodes.Add(doc.DocumentElement.ChildNodes[2]);
            lstNodes.Add(doc.DocumentElement.ChildNodes[3]);

            sut.Call_ReconstructWithNodes(lstNodes);

            Assert.That(doc.InnerXml,
                            Is.EqualTo("<root><child1 /></root>")
                        );

        }


        [Test]
        public void ReconstructWithNodes_NullListNodes_XmlDomWithoutChanges()
        {
            string xml = "<root><child2>test1</child2><child1 /><child2>test2</child2><child2>test3</child2></root>";
            XmlDocument doc = CreateXmlDocument(xml);

            string tagName = "child2";
            sut = CreateSUT(doc, tagName);

            sut.Call_ReconstructWithNodes(null);

            Assert.That(doc.InnerXml,
                            Is.EqualTo(xml)
                        );

        }


        [Test]
        public void ReconstructWithNodes_EmptyListNodes_XmlDomWithoutChanges()
        {
            string xml = "<root><child2>test1</child2><child1 /><child2>test2</child2><child2>test3</child2></root>";
            XmlDocument doc = CreateXmlDocument(xml);

            string tagName = "child2";
            sut = CreateSUT(doc, tagName);

            List<XmlNode> lstNodes = new List<XmlNode>();

            sut.Call_ReconstructWithNodes(lstNodes);

            Assert.That(doc.InnerXml,
                            Is.EqualTo(xml)
                        );

        }


        #endregion


        #region Help funcs

        private SutMock CreateSUT(XmlDocument doc, string tagName = null, bool isLocalName = true)
        {
            return new SutMock(doc, tagName, isLocalName);
        }

        private XmlDocument CreateXmlDocument(string xml) => XmlDocumentReconstructor_Tests.CreateXmlDocument(xml);

        #endregion



        #region Help class

        private class SutMock : XmlDocumentReconstructorRemoveByTagName
        {
            public SutMock(XmlDocument xmlDoc, string tagName = null, bool isLocalName = true) : base(xmlDoc, tagName, isLocalName)
            {
            }

            public bool Call_IsNodeToReconstruct(XmlNode node) => IsNodeToReconstruct(node);
            public void Call_ReconstructWithNodes(IList<XmlNode> nodes) => ReconstructWithNodes(nodes);

            protected override bool IsNodeToReconstruct(XmlNode node)
            {                        
                return base.IsNodeToReconstruct(node);
            }

            protected override void ReconstructWithNodes(IList<XmlNode> nodes)
            {
                base.ReconstructWithNodes(nodes);
            }
        }


        #endregion


    }
}