﻿using NSubstitute;

using NUnit.Framework;

using Sipol.IO;
using Sipol.XML.Interfaces;
using System.IO;
using System.IO.Abstractions;
using System.Xml;

namespace Sipol.XML.Tests
{
    [TestFixture()]
    public class XmlFileDocumentator_Tests
    {
        private XmlFileDocumentator sut;

        [SetUp]
        public void Init()
        {
            FS.Current = new SipolFileSys(true);
            FS.File = Substitute.For<IFile>();
            sut = null;
        }


        #region XmlDocument Tests

        [Test()]
        public void XmlDocument__byDefault__InstanceOfXmlDocument()
        {
            // Arrange
            string path = @"Z:\test\xmlfiles\some.xml";

            FS.File.Exists(path).Returns(true);
            FS.File.ReadAllText(path).Returns("<root/>");

            sut = CreateSUT(path);

            // Act
            var result = sut.XmlDocument;

            // Assert
            Assert.That(result,
                        Is.Not.Null &
                        Is.InstanceOf<XmlDocument>()
            );

        }


        [Test()]
        public void XmlDocument__byDefault__CallFSFileExistsOnXmlFilePath()
        {
            // Arrange
            string path = @"Z:\test\xmlfiles\some.xml";

            FS.File.Exists(path).Returns(true);
            FS.File.ReadAllText(path).Returns("<root/>");

            sut = CreateSUT(path);

            // Act
            var result = sut.XmlDocument;

            // Assert
            FS.File.Received(1).Exists(path);
        }

        [Test()]
        public void XmlDocument__WhenFileNotExists__ThrowFileNotFoundException()
        {
            // Arrange
            string path = @"Z:\test\xmlfiles\some.xml";

            FS.File.Exists(path).Returns(false);

            sut = CreateSUT(path);

            // Act
            // Assert
            Assert.That(() =>
                {
                    var result = sut.XmlDocument;
                },
                Throws.InstanceOf<FileNotFoundException>()
            );
        }

        [Test()]
        public void XmlDocument__byDefault__CallFSFileReadAllTextOnXmlFilePath()
        {
            // Arrange
            string path = @"Z:\test\xmlfiles\some.xml";

            FS.File.Exists(path).Returns(true);
            FS.File.ReadAllText(path).Returns("<root/>");

            sut = CreateSUT(path);

            // Act
            var result = sut.XmlDocument;

            // Assert
            FS.File.Received(1).ReadAllText(path);
        }

        #endregion


        #region Make Tests


        [Test()]
        public void Make__byDefault__InstanceNotNullAndXmlFileDocumentator()
        {
            // Arrange
            string path = @"Z:\test\xmlfiles\some.xml";

            // Act
            var result = XmlFileDocumentator.Make(path);

            // Assert
            Assert.That(result,
                            Is.Not.Null &
                            Is.InstanceOf<XmlFileDocumentator>()
            );
        }

        [TestCase(null)]
        [TestCase("")]
        [TestCase(" ")]
        [TestCase("   ")]
        public void Make__WhenXmlFilePathIsIncorrectString__ThrowArgumentNullException(string path)
        {
            // Arrange
            // Act
            // Assert
            Assert.That(() =>
                {
                    var result = XmlFileDocumentator.Make(path);
                },
                Throws.ArgumentNullException
            );
        }

        #endregion

        #region help funcs

        private XmlFileDocumentator CreateSUT(string xmlfilepath)
        {
            sut = new XmlFileDocumentator(xmlfilepath);
            return sut;
        }




        #endregion
    }
}