﻿using Sipol.XML.Interfaces;

using System;
using System.Xml;

namespace Sipol.XML
{
    public class XmlStringDocumentator : IXmlDocumentGetter, IXmlStringTransformer
    {
        private XmlDocument xmldoc = null;

        public XmlStringDocumentator( string xmlString )
        {
            XmlString = String.IsNullOrWhiteSpace(xmlString) ? throw new ArgumentNullException(nameof(xmlString)) : xmlString;
        }

        public string XmlString { get; private set; }

        public XmlDocument XmlDocument => GetXmlDocument();
        public IXmlStringTransformer XmlStringTransformer { get; set; } 

        public static IXmlDocumentGetter Make(string xmlstring, IXmlStringTransformer xmlStringTransformer = null)
        {
            return new XmlStringDocumentator(xmlstring) { XmlStringTransformer = xmlStringTransformer };
        }

        public string XmlStringTransform()
        {
            return XmlStringTransformer?.XmlStringTransform() ?? XmlString;
        }

        #region help funcs

        private XmlDocument GetXmlDocument()
        {
            if (xmldoc is null)
            {
                xmldoc = new XmlDocument();
                try
                {
                    xmldoc.LoadXml(XmlStringTransform());
                }
                catch (Exception ex)
                {
                    throw new Exception("Cannot load Xml document", ex);
                }
            }

            return xmldoc;
        }

        

        #endregion
    }
}
