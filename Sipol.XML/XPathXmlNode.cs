﻿using InfoVeriti.Net.Extensions.Xml;
using Sipol.XML.Interfaces;

using System;
using System.Xml;

namespace Sipol.XML
{
    public class XPathXmlNode: IXPathXmlNode
    {
        public XPathXmlNode(XmlNode node)
        {
            Node = node ?? throw new ArgumentNullException(nameof(node));
            if (!(Node is XmlElement) && !(Node is XmlAttribute)) throw new TypeAccessException(nameof(node) + " maybe olny is XmlElemnt or XmlAttribute type");
        }

        public int IndexInParent { get; protected set; } = -1;

        public virtual string XPath => GetXPath(false, true);

        public XmlNode Node { get; private set; } = null;

        public string NamespacePrefix { get; protected set; } = String.Empty;

        protected string GetXPath(bool useLocalNames, bool useIndexer)
        {
            int indexInParent = -1;
            string nsPrefix = String.Empty;
            string result = XmlDocumentExt.FindXPath(Node, out indexInParent, out nsPrefix, useLocalNames, useIndexer);
            IndexInParent = indexInParent;
            NamespacePrefix = nsPrefix;
            return result;
        }


    }
}
