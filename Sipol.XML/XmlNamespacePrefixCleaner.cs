﻿using Sipol.XML.Interfaces;

using System;
using System.Text.RegularExpressions;

namespace Sipol.XML
{
    public class XmlNamespacePrefixCleaner : IXmlStringTransformer
    {
        private IXmlStringTransformer innerTransformer;

        protected XmlNamespacePrefixCleaner( string xmlString, IXmlStringTransformer innerXmlStringTransformer = null )
        {
            if (String.IsNullOrWhiteSpace(xmlString))
                throw new ArgumentNullException(nameof(xmlString));

            XmlString = xmlString;
            innerTransformer = innerXmlStringTransformer;
        }

        public string XmlString { get; private set; }

        public string XmlStringTransform()
        {
            string xml = innerTransformer?.XmlStringTransform() ?? XmlString;
            return Regex.Replace(Regex.Replace(xml, "<([a-zA-Z0-9_]+):", "<"),"</([a-zA-Z0-9_]+):","</");
        }

        public static XmlNamespacePrefixCleaner Make(string xmlString, IXmlStringTransformer innerXmlStringTransformer = null )
        {
            return new XmlNamespacePrefixCleaner(xmlString, innerXmlStringTransformer);
        }
    }
}
