﻿using Sipol.IO;
using Sipol.XML.Interfaces;
using System.IO;
using System.Xml;

namespace Sipol.XML
{
    public class XmlFileDocumentatorWithoutNamespace : XmlFileDocumentator
    {
        private XmlDocument xmldoc = null;

        public XmlFileDocumentatorWithoutNamespace( string xmlFilePath ) : base(xmlFilePath)
        {
        }
            
        public new static XmlFileDocumentatorWithoutNamespace Make( string xmlFilePath )
        {
            return new XmlFileDocumentatorWithoutNamespace(xmlFilePath);
        }

        protected override XmlDocument GetXmlDocument()
        {
            if (xmldoc is null)
            {
                if (FS.File.Exists(XmlFilePath))
                {
                    string xmlstring = FS.File.ReadAllText(XmlFilePath);

                    IXmlStringTransformer xmlNSPrefixCleaner = XmlNamespaceAttributesCleaner.Make(xmlstring, XmlNamespacePrefixCleaner.Make(xmlstring));
                    IXmlDocumentGetter xmlStringDocumentator = XmlStringDocumentator.Make(xmlstring, xmlNSPrefixCleaner);

                    xmldoc = xmlStringDocumentator?.XmlDocument;
                }
                else
                    throw new FileNotFoundException("File ["+XmlFilePath+"] not found");
            }

            return xmldoc;
        }
    }
}
