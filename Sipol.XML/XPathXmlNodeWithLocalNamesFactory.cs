﻿using Sipol.XML.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Sipol.XML
{
    public class XPathXmlNodeWithLocalNamesFactory : IXPathXmlNodeFactory
    {
        public IXPathXmlNode CreateXPathXmlNode(XmlNode node)
        {
            return new XPathXmlNodeWithLocalNames(node);
        }
    }
}
