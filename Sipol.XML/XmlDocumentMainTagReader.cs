﻿using Sipol.XML.Interfaces;

using System;
using System.Xml;

namespace Sipol.XML
{
    public class XmlDocumentMainTagReader: IXmlMainTagReader, IXmlDocumentGetterFactory, IXmlDocumentGetter
    {
        private XmlDocument _XmlDocument = null;

        public XmlDocumentMainTagReader( IXmlDocumentGetter xmlDocumentGetter )
        {
            XmlDocumentGetter = xmlDocumentGetter ?? throw new ArgumentNullException(nameof(xmlDocumentGetter));
        }

        public XmlDocumentMainTagReader( XmlDocument xmldoc)
        {
            _XmlDocument = xmldoc ?? throw new ArgumentNullException(nameof(xmldoc));
        }

        public IXmlDocumentGetter XmlDocumentGetter { get; private set; }

        public XmlDocument XmlDocument => XmlDocumentGetter?.XmlDocument;

        public string MainTag => GetMainTag();

        public string GetMainTag()
        {
            if (XmlDocumentGetter is null)
                CreateXmlDocumentGetter();

            return XmlDocument?.DocumentElement?.LocalName ?? String.Empty;
        }

        public IXmlDocumentGetter CreateXmlDocumentGetter()
        {
            IXmlDocumentGetter documentGetter = new XmlDocGetterHelper(_XmlDocument);
            XmlDocumentGetter = documentGetter;
            return documentGetter;
        }

        #region help class

        public class XmlDocGetterHelper : IXmlDocumentGetter
        {
            public XmlDocGetterHelper( XmlDocument xmlDocument )
            {
                XmlDocument = xmlDocument ?? throw new ArgumentNullException(nameof(xmlDocument));
            }

            public XmlDocument XmlDocument { get; private set; }
        }


        #endregion
    }
}
