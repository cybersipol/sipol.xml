﻿using Sipol.XML.Interfaces;
using System;
using System.Text.RegularExpressions;

namespace Sipol.XML
{
    public class XPathMainTagReader : IXmlMainTagReader
    {
        private String XPath;

        public XPathMainTagReader( string xPath )
        {
            XPath = xPath;
        }

        public string GetMainTag()
        {
            if (String.IsNullOrWhiteSpace(XPath))
                return String.Empty;

            if (!Regex.IsMatch(XPath, "^/"))
                return String.Empty;


            string[] tags = XPath.Trim('/').Split('/');
            return tags[0];
        }


        public static string GetMainTag(string XPath)
        {
            return new XPathMainTagReader(XPath).GetMainTag();
        }
    }
}
