﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace Sipol.XML
{
    public class XPathXmlNodeShort : XPathXmlNode
    {
        public XPathXmlNodeShort(XmlNode node) : base(node) { }

        public override string XPath => base.GetXPath(true, false);
    }
}