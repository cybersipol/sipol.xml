﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Sipol.XML
{
    public class XmlTagNameWithType
    {
        private string _tagname;
        private string _type;

        public XmlTagNameWithType()
        {
        }

        public XmlTagNameWithType( string tagName, string type = null)
        {
            TagName = tagName;
            Type = type;
        }

        public String TagName { get => _tagname ?? String.Empty; set => SetTagName(value); }
        public String Type { get => _type?.Trim() ?? String.Empty; set => _type = value; }

        public override bool Equals( object obj )
        {
            if (obj is null)
                return false;
            if ((obj is XmlTagNameWithType) == false)
                return false;

            return Equals(obj as XmlTagNameWithType);
        }

        public bool Equals(XmlTagNameWithType tag)
        {
            if (tag == null)
                return false;

            bool isCorerectType = true;

            if (String.IsNullOrWhiteSpace(Type) == false && String.IsNullOrWhiteSpace(tag.Type) == false)
                isCorerectType = tag.Type.Equals(Type);

            return tag.TagName.Equals(TagName) && isCorerectType;
        }

        public override int GetHashCode()
        {
            return TagName.GetHashCode();
        }

        public override string ToString()
        {
            return $"<{TagName}>";
        }

        private void SetTagName( string val )
        {
            if (String.IsNullOrWhiteSpace(val))
            {
                _tagname = null;
                return;
            }

            string v = val.Trim();
            v = Regex.Replace(v, @"([^a-zA-Z0-9\\-\\_\\:\\.]+)", "");
            v = Regex.Replace(v, @"^([0-9\\-\\:\\.]+)", "");

            int lastIndex = v.LastIndexOf(':');
            if (lastIndex > 0)
            {
                v = v.Replace(':', ' ');
                char[] arr = v.ToCharArray();
                arr[lastIndex] = ':';
                v = new string(arr);
                v = v.Replace(" ", String.Empty);
            }

            _tagname = v.Trim();
        }


        public static implicit operator String( XmlTagNameWithType tag)
        {
            return tag.TagName;
        }

        public static implicit operator XmlTagNameWithType( String text )
        {
            return new XmlTagNameWithType() { TagName = text };
        }
    }
}
