﻿using Sipol.XML.Interfaces;

using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Sipol.XML
{
    public class XmlNamespaceAttributesCleaner : IXmlStringTransformer
    {
        private IXmlStringTransformer innerTransformer = null;

        private XmlNamespaceAttributesCleaner( string xmlString, IXmlStringTransformer innerXmlStringTransformer = null)
        {
            XmlString = xmlString ?? throw new ArgumentNullException(nameof(xmlString));
            innerTransformer = innerXmlStringTransformer;
        }

        public string XmlString { get; private set; }

        public string XmlStringTransform()
        {
            var prefixesMatches = Regex.Matches(XmlString, " xmlns:([a-zA-Z0-9_]+)=\"([^\"]*)\"");

            List<string> prefixes = new List<string>();
            foreach (Match m in prefixesMatches)
            {
                if (m.Success == false)
                    continue;

                prefixes.Add(m.Groups[1].Value);
            }

            string xml = innerTransformer?.XmlStringTransform() ?? XmlString;
            foreach (string prefix in prefixes)
                xml = Regex.Replace(Regex.Replace(xml, " xmlns:" + prefix + "=\"([^\"]*)\"", ""), " " + prefix + ":([a-zA-Z0-9_]+)=", " $1=");


            return Regex.Replace(xml, " xmlns=\"([^\"]*)\"", "");
        }

        public static XmlNamespaceAttributesCleaner Make(string xmlString, IXmlStringTransformer innerXmlStringTransformer = null)
        {
            return new XmlNamespaceAttributesCleaner(xmlString, innerXmlStringTransformer);
        }

    }
}
