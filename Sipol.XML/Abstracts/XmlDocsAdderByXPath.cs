﻿using InfoVeriti.Net.Extensions.Xml;
using Sipol.XML.Interfaces;

using System;
using System.Collections.Generic;
using System.Xml;

namespace Sipol.XML.Abstracts
{
    public abstract class XmlDocsAdderByXPath: IXmlDocsAdder
    {
        public XmlDocument FirstXmlDocument { get; private set; } = null;
        public XmlDocument SecondXmlDocument { get; private set; } = null;

        public IXPathXmlNodeFactory XPathNodeFactory { get; protected set; } = null;

        protected Dictionary<string, XmlNode> XPathDocList { get; private set; } = null;

        protected XmlDocsAdderByXPath( XmlDocument firstXmlDocument, XmlDocument secondXmlDocument, IXPathXmlNodeFactory xPathNodeFactory = null)
        {
            FirstXmlDocument = firstXmlDocument ?? throw new ArgumentNullException(nameof(firstXmlDocument));
            SecondXmlDocument = secondXmlDocument ?? throw new ArgumentNullException(nameof(secondXmlDocument));
            XPathNodeFactory = xPathNodeFactory;

            CreateDefaultXPathNodeFactory();
            XPathDocList = new Dictionary<string, XmlNode>();
        }

        public abstract void AdditionXmlDocuments();

        protected virtual void CreateDefaultXPathNodeFactory()
        {
            if (XPathNodeFactory == null)
                XPathNodeFactory = new XPathXmlNodeFactory();
        }

        protected void CreateXPathFromXmlDocument(XmlDocument doc)
        {            
            XPathDocList.Clear();
            if (doc == null)
                return;

            if (!doc.HasChildNodes)
                return;

            CreateDefaultXPathNodeFactory();

            doc.IterateThroughAllNodes(n =>
            {
                if (n == null) return;
                if (n.NodeType != XmlNodeType.Element) return;

                string xpath = XPathNodeFactory?.CreateXPathXmlNode(n)?.XPath;

                if (!String.IsNullOrWhiteSpace(xpath))
                    XPathDocList.Add(xpath, n);
            });

        }
    }
}
