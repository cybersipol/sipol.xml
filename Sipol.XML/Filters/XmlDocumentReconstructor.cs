﻿using InfoVeriti.Net.Extensions.Xml;
using Sipol.XML.Filters.Interfaces;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;

namespace Sipol.XML.Filters
{
    /// <summary>
    /// Template Method for XmlDocument Reconstructor
    /// </summary>
    public abstract class XmlDocumentReconstructor : IXmlDocumentReconstructor
    {
        public XmlDocument XmlDoc { get; private set; } = null;

        protected XmlDocumentReconstructor(XmlDocument xmlDoc)
        {
            XmlDoc = xmlDoc ?? throw new ArgumentNullException(nameof(xmlDoc));
        }

        protected abstract bool IsNodeToReconstruct(XmlNode node);
        protected abstract void ReconstructWithNodes(IList<XmlNode> nodes);


        public void RemoveNodes(IList<XmlNode> nodes)
        {
            if (nodes == null) return;
            if (nodes.Count <= 0) return;

            foreach (XmlNode node in nodes)
            {
                if (node!=null && node.OwnerDocument==XmlDoc)
                {
                    if (node.ParentNode!=null)
                        node.ParentNode.RemoveChild(node);
                }
            }
        }

        public void RecontructXmlDocument()
        {
            List<XmlNode> nodesToReconstruct = new List<XmlNode>();
            
            XmlDocumentExt.IterateThroughAllNodes(XmlDoc, delegate (XmlNode node)
            {
                if (IsNodeToReconstruct(node))
                    nodesToReconstruct.Add(node);
            });

            if (nodesToReconstruct.Count > 0)
                ReconstructWithNodesLevelDescSorting(nodesToReconstruct);
        }

        private void ReconstructWithNodesLevelDescSorting(List<XmlNode> nodesToReconstruct)
        {
            ReconstructWithNodes((from n in nodesToReconstruct
                                  orderby XmlDoc.GetNodeLevel(n) descending, n.Name
                                  select n).ToList());
        }
    }
}
