﻿using System.Xml;

namespace Sipol.XML.Filters.Interfaces
{
    public interface IXmlNodeFilter
    {
        bool IsXmlNodeAccepted(XmlNode node);
    }
}
