﻿using System.Xml;

namespace Sipol.XML.Filters.Interfaces
{
    public interface IXmlDocumentReconstructor
    {
        XmlDocument XmlDoc { get; }

        void RecontructXmlDocument();
    }
}
