﻿using Sipol.XML.Filters.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Sipol.XML.Filters
{
    public class XmlDocumentReconstructorRemoveWithFilter : XmlDocumentReconstructor, IXmlDocumentReconstructor
    {
        public IXmlNodeFilter Filter { get; set; } = null;

        public XmlDocumentReconstructorRemoveWithFilter(XmlDocument doc, IXmlNodeFilter filter = null) 
            : base(doc)
        {
            Filter = filter;
        }


        protected override bool IsNodeToReconstruct( XmlNode node )
        {
            if (node == null)
                return false;

            if (Filter == null)
                return false;

            return Filter.IsXmlNodeAccepted(node);
        }


        protected override void ReconstructWithNodes( IList<XmlNode> nodes )
        {
            RemoveNodes(nodes);
        }
    }
}
