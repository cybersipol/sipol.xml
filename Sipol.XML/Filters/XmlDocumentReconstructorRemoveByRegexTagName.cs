﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;

namespace Sipol.XML.Filters
{
    public class XmlDocumentReconstructorRemoveByRegexTagName : XmlDocumentReconstructor
    {
        public string RegexTagName { get; set; } = null;
        public bool IsLocalName { get; set; } = true;

        public XmlDocumentReconstructorRemoveByRegexTagName(XmlDocument xmlDocument, string regexTagName=null, bool isLocalName=true): base(xmlDocument)
        {
            RegexTagName = regexTagName;
            IsLocalName = isLocalName;
        }


        protected override bool IsNodeToReconstruct(XmlNode node)
        {
            if (node == null)
                return false;

            if (String.IsNullOrWhiteSpace(RegexTagName))
                return false;

            string nodeName = (IsLocalName ? node.LocalName : node.Name);

            if (Regex.IsMatch(nodeName, RegexTagName))
                return true;

            return false;
        }


        protected override void ReconstructWithNodes(IList<XmlNode> nodes)
        {
            RemoveNodes(nodes);
        }
    }
}
