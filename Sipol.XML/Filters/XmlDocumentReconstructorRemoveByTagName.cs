﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace Sipol.XML.Filters
{
    public class XmlDocumentReconstructorRemoveByTagName : XmlDocumentReconstructor
    {
        public string TagName { get; set; } = null;
        public bool IsLocalName { get; set; } = true;

        public XmlDocumentReconstructorRemoveByTagName(XmlDocument xmlDoc, string tagName = null, bool isLocalName = true) : base(xmlDoc)
        {
            TagName = tagName;
            IsLocalName = isLocalName;
        }

        protected override bool IsNodeToReconstruct(XmlNode node)
        {
            if (node == null) return false;
            if (String.IsNullOrWhiteSpace(TagName)) return false;

            if (node.OwnerDocument != XmlDoc) return false;

            string nodeName = (IsLocalName ? node.LocalName : node.Name);

            if (String.IsNullOrWhiteSpace(nodeName)) return false;
            return nodeName.Equals(TagName);
        }

        protected override void ReconstructWithNodes(IList<XmlNode> nodes)
        {            
            RemoveNodes(nodes);
        }
    }
}