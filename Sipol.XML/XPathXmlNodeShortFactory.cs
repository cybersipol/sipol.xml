﻿using Sipol.XML.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Sipol.XML
{
    /// <summary>
    /// Create XPathXmlNode with localnames and without index
    /// </summary>

    public class XPathXmlNodeShortFactory : IXPathXmlNodeFactory
    {
        public IXPathXmlNode CreateXPathXmlNode(XmlNode node)
        {
            return new XPathXmlNodeShort(node);
        }
    }
}
