﻿namespace Sipol.XML.Interfaces
{
    public interface IXmlMainTagReader
    {
        string GetMainTag();
    }
}