﻿using System.Xml;

namespace Sipol.XML.Interfaces
{
    public interface IXmlDocumentGetter
    {
        XmlDocument XmlDocument { get; }
    }
}
