﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Sipol.XML.Interfaces
{
    public interface IXPathXmlNode
    {
        int IndexInParent { get; }
        string NamespacePrefix { get; }

        string XPath { get; }

        XmlNode Node { get; }
    }
}
