﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sipol.XML.Interfaces
{
    public interface IToXmlString
    {
        string ToXmlString();
    }
}
