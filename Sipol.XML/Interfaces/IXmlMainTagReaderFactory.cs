﻿namespace Sipol.XML.Interfaces
{
    public interface IXmlMainTagReaderFactory
    {
        IXmlMainTagReader MakeXmlMainTagReader();
    }
}
