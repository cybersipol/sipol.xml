﻿namespace Sipol.XML.Interfaces
{
    public interface IXmlStringTransformer
    {
        string XmlStringTransform();
    }
}
