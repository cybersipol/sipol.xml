﻿namespace Sipol.XML.Interfaces
{
    public interface IXmlDocumentGetterFactory
    {
        IXmlDocumentGetter CreateXmlDocumentGetter();
    }
}
