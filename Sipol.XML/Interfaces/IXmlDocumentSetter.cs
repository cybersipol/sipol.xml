﻿using System.Xml;

namespace Sipol.XML.Interfaces
{
    public interface IXmlDocumentSetter
    {
        XmlDocument XmlDocument { set; }
    }
}
