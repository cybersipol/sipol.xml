﻿using System.Xml;

namespace Sipol.XML.Interfaces
{
    public interface IXmlDocumentOwner: IXmlDocumentSetter, IXmlDocumentGetter
    {
        new XmlDocument XmlDocument { get; set;  }
    }
}
