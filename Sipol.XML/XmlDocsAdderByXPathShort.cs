﻿using InfoVeriti.Net.Extensions.Xml;
using Sipol.XML.Abstracts;
using Sipol.XML.Interfaces;

using System;
using System.Xml;

namespace Sipol.XML
{
    public class XmlDocsAdderByXPathShort : XmlDocsAdderByXPath
    {
        public event Action<XmlNode, XmlNode> OnNodeAddition;


        public XmlDocsAdderByXPathShort( XmlDocument firstXmlDocument, XmlDocument secondXmlDocument, IXPathXmlNodeFactory xPathNodeFactory = null ) : base(firstXmlDocument, secondXmlDocument, xPathNodeFactory)
        {
        }

        public override void AdditionXmlDocuments()
        {
            CreateXPathFromXmlDocument(FirstXmlDocument);

            if (XPathDocList == null)
                return;

            if (XPathDocList.Count <= 0)
                return;

            if (!SecondXmlDocument.HasChildNodes)
                return;

            CreateDefaultXPathNodeFactory();

            SecondXmlDocument.IterateThroughAllNodes(node =>
            {
                if (node == null)
                    return;
                if (node.NodeType != XmlNodeType.Element)
                    return;

                string xpath = XPathNodeFactory?.CreateXPathXmlNode(node)?.XPath;

                if (String.IsNullOrWhiteSpace(xpath)) return;

                if (XPathDocList.ContainsKey(xpath))
                    OnNodeAddition?.Invoke(XPathDocList[xpath], node);
            }                
            );

            
        }

        protected override void CreateDefaultXPathNodeFactory()
        {
            if (XPathNodeFactory == null)
                XPathNodeFactory = new XPathXmlNodeShortFactory();
        }
    }
}
