﻿using InfoVeriti.Net.Extensions.Xml;
using Sipol.XML.Interfaces;

using System;
using System.Collections.Generic;
using System.Xml;

namespace Sipol.XML
{
    public class XPathXmlElementMapper : IXPathMapper<XmlElement>
    {
        private XmlDocument xmlDocument = null;
        private IXPathXmlNodeFactory XPathXmlNodeFacotry = null;
        private bool isCreateMap = false;

        public XPathXmlElementMapper(XmlDocument xmlDocument, IXPathXmlNodeFactory xPathXmlNodeFactory)
        {
            this.xmlDocument = xmlDocument ?? throw new ArgumentNullException(nameof(xmlDocument));
            this.XPathXmlNodeFacotry = xPathXmlNodeFactory ?? throw new ArgumentNullException(nameof(xPathXmlNodeFactory));
        }

        private SortedDictionary<string, XmlElement> xpathXmlElemList = new SortedDictionary<string, XmlElement>();

        public int Count => xpathXmlElemList.Count;

        public void CreateMap()
        {
            if (isCreateMap) return;

            XmlDocumentExt.IterateThroughAllNodes(xmlDocument, delegate (XmlNode node)
            {
                AddXPathXmlElem(GetXPathForXmlNode(node), node);
            });

            isCreateMap = true;
        }

        public XmlElement Get(string XPath)
        {
            if (!isCreateMap) throw new Exception("No created Map");
            return xpathXmlElemList.ContainsKey(XPath) ? xpathXmlElemList[XPath] : null;
        }

        private void AddXPathXmlElem(string xpath, XmlNode element)
        {
            if (IsXmlElement(element) && IsXPathOkToAdd(xpath))
                xpathXmlElemList.Add(xpath, (XmlElement)element);
        }

        private bool IsXmlElement(XmlNode node) => node != null && node is XmlElement;

        private bool IsXPathOkToAdd(string xpath) => !String.IsNullOrEmpty(xpath) && !IsXPathExists(xpath);

        private bool IsXPathExists(string xpath) => xpathXmlElemList.ContainsKey(xpath);

        private string GetXPathForXmlNode(XmlNode node) => IsXmlElement(node) ? XPathXmlNodeFacotry.CreateXPathXmlNode(node).XPath : null;
    }
}
