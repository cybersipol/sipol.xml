﻿using Sipol.IO;
using Sipol.XML.Interfaces;

using System;
using System.IO;
using System.Xml;

namespace Sipol.XML
{
    public class XmlFileDocumentator : IXmlDocumentGetter
    {
        private XmlDocument xmldoc = null;

        public XmlFileDocumentator( string xmlFilePath )
        {
            if (String.IsNullOrWhiteSpace(xmlFilePath))
                throw new ArgumentNullException(nameof(xmlFilePath));

            XmlFilePath = xmlFilePath;
        }

        public string XmlFilePath { get; private set; }

        public XmlDocument XmlDocument { get => GetXmlDocument(); }

        public static XmlFileDocumentator Make( string xmlFilePath )
        {
            return new XmlFileDocumentator(xmlFilePath);
        }


        #region help funcs

        protected virtual XmlDocument GetXmlDocument()
        {
            if (xmldoc is null)
            {
                if (FS.File.Exists(XmlFilePath))
                {
                    string xmlstring = FS.File.ReadAllText(XmlFilePath);

                    IXmlDocumentGetter xmlStringDocumentator = XmlStringDocumentator.Make(xmlstring, null);

                    xmldoc = xmlStringDocumentator?.XmlDocument;
                }
                else throw new FileNotFoundException("File ["+XmlFilePath+"] not found");
            }

            return xmldoc;    

        }
        #endregion


    }
}
